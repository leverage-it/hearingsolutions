const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

var babelLoader = {test: /\.js$/, loader: 'babel-loader', exclude:/node_modules/, query: {presets: ['es2015']}};
var rawLoader = {test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/};
var styleLoader = {test: /\.css$/, loader: "style-loader!css-loader", exclude: /node_modules/};
var scssLoader = {test: /\.scss$/, loader: "style-loader!css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap", exclude: /node_modules/};
var scssLoaderExtracted = {test: /\.scss$/, use: ExtractTextPlugin.extract({
  fallback: "style-loader",
  use: "css-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap"
}), exclude: /node_modules/};
var urlLoader = {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader'};

var defaultModuleRules = [
  babelLoader,
  rawLoader,
  urlLoader
];

var CSSModuleRules = defaultModuleRules.slice();
CSSModuleRules.push(styleLoader, scssLoader);

var extractedCSSModuleRules = defaultModuleRules.slice();
extractedCSSModuleRules.push(styleLoader, scssLoaderExtracted);

var config = {
  watch: true,
  devtool: 'source-map'
};

var criticalConfig = Object.assign({}, config, {
  name: "critical",
  entry: __dirname + "/src/entry/critical.js",
  output: {
    path: __dirname + "/js",
    filename: "critical.js"
  },
  module: {
      rules: CSSModuleRules
    },
});

var generalConfig = Object.assign({}, config, {
  name: "general",
  entry: __dirname + "/src/entry/app.js",
  output: {
    path: __dirname,
    filename: "js/app.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/styles.css')
  ],
});

var adminConfig = Object.assign({}, config, {
  name: "general",
  entry: __dirname + "/src/entry/admin.js",
  output: {
    path: __dirname,
    filename: "js/admin.js"
  },
  module: {
      rules: extractedCSSModuleRules
  },
  plugins: [
    new ExtractTextPlugin('css/styles-admin.css')
  ],
});

var mapConfig = Object.assign({}, config, {
  name: "map",
  entry: __dirname + "/src/entry/map.js",
  output: {
    path: __dirname + "/js",
    filename: "map.js"
  },
  module: {
      rules: defaultModuleRules
  },
});

var slickConfig = Object.assign({}, config, {
  name: "slick",
  entry: __dirname + "/src/entry/slick.js",
  output: {
    path: __dirname + "/js",
    filename: "slick.js"
  },
  module: {
    rules: defaultModuleRules
  },
});

module.exports = [
  criticalConfig, generalConfig, adminConfig, mapConfig, slickConfig
];
