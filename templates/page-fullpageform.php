<?php

/*
Template Name: Full Page Form Page
Template Post Type: page, careers
*/

get_header(); ?>

<?php

  if(have_posts()) :
  while (have_posts()) : the_post(); ?>

  <section class="container-fluid page-header-container jarallax">
    <?php the_post_thumbnail('full', ['class' => 'jarallax-img page-header-image']); ?>
    <div class="row breadcrumb-container-transparent">
      <?php get_template_part('template-parts/breadcrumb'); ?>
    </div>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col col-12 page-title-container">
            <div class="page-title">
              <h1 class="page-title-text">
                <?php
                  if (get_field('long_title')) :
                  the_field('long_title');
                  elseif (get_field('career_long_title')):
                    the_field('career_long_title');
                  else :
                    the_title();
                  endif;
                ?>
              </h1>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 page-excerpt-container">
            <div class="page-excerpt">
              <h4><?php the_excerpt(); ?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container-fluid main-container template-fullpageform-main <?php  if (get_post_type() == 'careers') echo 'careers-main-container'; ?>">
    <div class="row">
      <div class="container content-container">
        <div class="row">
          <div class="col col-12 page-container-content-container">
            <div class="content">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
    if (get_post_type() != 'careers') :
  ?>
  <section class="container-fluid main-container template-fullpageform-bottom">
    <div class="row">
      <div class="container">
        <div class="row justify-content-lg-end">
          <div class="col col-12 page-share-container">
            <?php echo crunchify_social_sharing_buttons(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
    else :
      get_template_part('template-parts/careers-features');
    endif;
    endwhile;
  else:
    echo '<p>Sorry, no content found</p>';
  endif;
get_footer();
?>
