<?php

/*
Template Name: Home Page
*/

get_header();

  if(have_posts()) :
    while (have_posts()) : the_post();
 ?>

  <section class="container-fluid hero-container">
    <div class="row">
      <div class="left" style="background-image: url(<?php the_field('hero_image_1'); ?>)">
        <div class="hero-left-text-container">
          <h1><?php the_field('hero_title_1'); ?></h1>
          <h2><?php the_field('hero_subtitle_1'); ?></h2>
          <a href="<?php the_field('hero_button_link_1')?>" class="button-yellow"><?php the_field('hero_button_text_1'); ?></a>
        </div>
      </div>
      <div class="right">
        <a class="right-1" href="<?php the_field('hero_button_link_2')?>">
          <img src="<?php the_field('hero_image_2'); ?>" class="background-image" alt="">
          <div class="hero-right-text-container">
            <h1><?php the_field('hero_title_2'); ?></h1>
            <h2><?php the_field('hero_subtitle_2'); ?></h2>
          </div>
        </a>
        <a class="right-2" href="<?php the_field('hero_button_link_3')?>">
          <img src="<?php the_field('hero_image_3'); ?>" class="background-image" alt="">
          <div class="hero-right-text-container">
            <h1><?php the_field('hero_title_3'); ?></h1>
            <h2><?php the_field('hero_subtitle_3'); ?></h2>
          </div>
        </a>
      </div>
    </div>
  </section>
  <section class="container-fluid for-container offset-container">
    <div class="row">
      <div class="container sheet-container top">
        <div class="row">
          <div class="col col-12 col-md-12 col-lg-6" id="sheet-1-left">
            <div class="sheet-text-container">
              <img class="logo-icon" src="<?php echo get_template_directory_uri(); ?>/assets/icon.svg"  alt="">
              <div class="section-header">
                <h3><?php the_field('main_section_1_title'); ?></h3>
                <div class="line"></div>
              </div>
              <p><?php the_field('main_section_1_text'); ?></p>
              <a href="<?php the_field('main_section_1_button_link')?>" class="button-blue"><?php the_field('main_section_1_button_text'); ?></a>
            </div>
          </div>
          <div class="col col-12 col-md-12 col-lg-6" id="sheet-1-right"  style="background-image: url(<?php the_field('main_section_1_video_preview_image'); ?>)">
            <a href="<?php the_field('main_section_1_video_url'); ?>" class="wplightbox button-play" data-width="640" data-height="360">
              <i class="fas fa-play"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container-fluid slider-container">
    <div class="row">
      <div id="home-slider">
        <?php
          if(have_rows('home_slideshow')):
            while(have_rows('home_slideshow')):the_row();
        ?>
          <div class="home-slide" style="background-image:url(<?php the_sub_field('home_slideshow_image'); ?>);">
              <div class="container">
                <div class="row">
                  <div class="col col-12 slider-inner">
                    <h1><?php the_sub_field('home_slideshow_title'); ?></h1>
                    <h2><?php the_sub_field('home_slideshow_text'); ?></h2>
                    <a class="button-blue" href="<?php the_sub_field('home_slideshow_button_url'); ?>"><?php the_sub_field('home_slideshow_button_text'); ?></a>
                  </div>
                </div>
              </div>
          </div>
        <?php
            endwhile;
          else:
          endif;
        ?>
      </div>
    </div>
  </section>
  <section class="container-fluid for-container offset-container">
    <div class="row">
      <div class="container sheet-container bottom" id="sheet-2">
        <div class="row">
          <div class="col col-12 sheet-title-container">
            <div class="section-header">
              <h3><?php the_field('main_section_2_title'); ?></h3>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-12 col-lg-4 feature-container">
            <div class="image-container">
              <a href="<?php the_field('main_section_2_feature_1_link'); ?>">
                <img src="<?php the_field('main_section_2_feature_1_image'); ?>"  alt=""/>
              </a>
            </div>
            <div class="text-container">
              <h4>
                <a href="<?php the_field('main_section_2_feature_1_link'); ?>"><?php the_field('main_section_2_feature_1_title'); ?></a>
              </h4>
              <p><?php the_field('main_section_2_feature_1_text'); ?></p>
            </div>
          </div>
          <div class="col col-12 col-md-12 col-lg-4 feature-container">
            <div class="image-container">
              <a href="<?php the_field('main_section_2_feature_2_link'); ?>">
                <img src="<?php the_field('main_section_2_feature_2_image'); ?>"  alt=""/>
              </a>
            </div>
            <div class="text-container">
              <h4>
                <a href="<?php the_field('main_section_2_feature_2_link'); ?>"><?php the_field('main_section_2_feature_2_title'); ?></a>
              </h4>
              <p><?php the_field('main_section_2_feature_2_text'); ?></p>
            </div>
          </div>
          <div class="col col-12 col-md-12 col-lg-4 feature-container">
            <div class="image-container">
              <a href="<?php the_field('main_section_2_feature_3_link'); ?>">
                <img src="<?php the_field('main_section_2_feature_3_image'); ?>"  alt=""/>
              </a>
            </div>
            <div class="text-container">
              <h4>
                <a href="<?php the_field('main_section_2_feature_3_link'); ?>"><?php the_field('main_section_2_feature_3_title'); ?></a>
              </h4>
              <p><?php the_field('main_section_2_feature_3_text'); ?></p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 button-container">
            <a href="<?php the_field('main_section_2_button_link'); ?>" class="button-blue"><?php the_field('main_section_2_button_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container-fluid tile-buttons-container">
    <div class="row">
      <?php
        $tile_1_link_choice = get_field_object('tile_1_link_choice');
        $tile_1_link_choice_value = $tile_1_link_choice['value'];
        $tile_2_link_choice = get_field_object('tile_2_link_choice');
        $tile_2_link_choice_value = $tile_2_link_choice['value'];
        $tile_3_link_choice = get_field_object('tile_3_link_choice');
        $tile_3_link_choice_value = $tile_3_link_choice['value'];
        $tile_4_link_choice = get_field_object('tile_4_link_choice');
        $tile_4_link_choice_value = $tile_4_link_choice['value'];
        $tile_5_link_choice = get_field_object('tile_5_link_choice');
        $tile_5_link_choice_value = $tile_5_link_choice['value'];
        $tile_6_link_choice = get_field_object('tile_6_link_choice');
        $tile_6_link_choice_value = $tile_6_link_choice['value'];
      ?>
      <a href="<?php if($tile_1_link_choice_value == 'Internal'): the_field('tile_1_internal_link'); elseif($tile_1_link_choice_value == 'External'): the_field('tile_1_external_link'); endif; ?>" target="<?php if($tile_1_link_choice_value == 'Internal'): echo "_self"; elseif($tile_1_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_1_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_1_text'); ?></h1>
      </a>
      <a href="<?php if($tile_2_link_choice_value == 'Internal'): the_field('tile_2_internal_link'); elseif($tile_2_link_choice_value == 'External'): the_field('tile_2_external_link'); endif; ?>" target="<?php if($tile_2_link_choice_value == 'Internal'): echo "_self"; elseif($tile_2_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_2_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_2_text'); ?></h1>
      </a>
      <a href="<?php if($tile_3_link_choice_value == 'Internal'): the_field('tile_3_internal_link'); elseif($tile_3_link_choice_value == 'External'): the_field('tile_3_external_link'); endif; ?>" target="<?php if($tile_3_link_choice_value == 'Internal'): echo "_self"; elseif($tile_3_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_3_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_3_text'); ?></h1>
      </a>
      <a href="<?php if($tile_4_link_choice_value == 'Internal'): the_field('tile_4_internal_link'); elseif($tile_4_link_choice_value == 'External'): the_field('tile_4_external_link'); endif; ?>" target="<?php if($tile_4_link_choice_value == 'Internal'): echo "_self"; elseif($tile_4_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_4_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_4_text'); ?></h1>
      </a>
      <a href="<?php if($tile_5_link_choice_value == 'Internal'): the_field('tile_5_internal_link'); elseif($tile_5_link_choice_value == 'External'): the_field('tile_5_external_link'); endif; ?>" target="<?php if($tile_5_link_choice_value == 'Internal'): echo "_self"; elseif($tile_5_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_5_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_5_text'); ?></h1>
      </a>
      <a href="<?php if($tile_6_link_choice_value == 'Internal'): the_field('tile_6_internal_link'); elseif($tile_6_link_choice_value == 'External'): the_field('tile_6_external_link'); endif; ?>" target="<?php if($tile_6_link_choice_value == 'Internal'): echo "_self"; elseif($tile_6_link_choice_value == 'External'): echo "_blank"; endif; ?>" class="col col-12 col-md-6 col-lg-4 button-tile">
        <img src="<?php  the_field('tile_6_image'); ?>" class="background-image" alt="">
        <h1><?php the_field('tile_6_text'); ?></h1>
      </a>
    </div>
  </section>
  <section class="container-fluid for-container offset-container">
    <div class="row">
      <div class="container sheet-container bottom" id="sheet-3">
        <div class="row">
          <div class="col col-12 sheet-title-container">
            <div class="section-header">
              <h3><?php the_field('main_section_3_title'); ?></h3>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12">
            <div class="container">
              <div class="row">
                <div class="col col-12 feature-container">
                  <div class="image-container">
                    <img src="<?php the_field('main_section_3_feature_1_image'); ?>" alt="" />
                  </div>
                  <div class="text-container">
                    <h3><?php the_field('main_section_3_feature_1_title'); ?></h3>
                    <p><?php the_field('main_section_3_feature_1_text'); ?></p>
                  </div>
                </div>
                <div class="col col-12 feature-container">
                  <div class="image-container">
                    <img src="<?php the_field('main_section_3_feature_2_image'); ?>"  alt=""/>
                  </div>
                  <div class="text-container">
                    <h3><?php the_field('main_section_3_feature_2_title'); ?></h3>
                    <p><?php the_field('main_section_3_feature_2_text'); ?></p>
                  </div>
                </div>
                <div class="col col-12 feature-container">
                  <div class="image-container">
                    <img src="<?php the_field('main_section_3_feature_3_image'); ?>" alt="" />
                  </div>
                  <div class="text-container">
                    <h3><?php the_field('main_section_3_feature_3_title'); ?></h3>
                    <p><?php the_field('main_section_3_feature_3_text'); ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 button-container">
            <a href="<?php the_field('main_section_3_button_link'); ?>" class="button-blue"><?php the_field('main_section_3_button_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php

endwhile;

else:
endif;

get_footer();

?>
