<?php

/*
Template Name: Download Page
*/

get_header(); ?>

<?php

  if(have_posts()) :
  while (have_posts()) : the_post(); ?>

  <section class="container-fluid page-header-container jarallax">
    <?php the_post_thumbnail('full', ['class' => 'jarallax-img page-header-image']); ?>
    <div class="row breadcrumb-container-transparent">
      <?php get_template_part('template-parts/breadcrumb'); ?>
    </div>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col col-12 page-title-container">
            <div class="page-title">
              <h1 class="page-title-text">
                <?php
                  if (get_field('long_title')) :
                  the_field('long_title');
                  elseif (get_field('career_long_title')):
                    the_field('career_long_title');
                  else :
                    the_title();
                  endif;
                ?>
              </h1>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 page-excerpt-container">
            <div class="page-excerpt">
              <h4><?php the_excerpt(); ?></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container-fluid main-container template-fullpageform-main">
    <div class="row">
      <div class="container content-container">
        <div class="row download-links-row-container">
          <div class="col col-12">
            <div class="container">
              <div class="row">
                <div class="col col-12 col-lg-4 download-links-content-container">
                  <?php
                    $download_1_link_choice = get_field_object('download_item_1_link_choice');
                    $download_1_link_choice_value = $download_1_link_choice['value'];
                  ?>
                  <a class="download-card" href="<?php if($download_1_link_choice_value == 'Internal'): the_field('download_item_1_internal_link'); elseif ($download_1_link_choice_value == 'External'): the_field('download_item_1_external_link'); endif; ?>">
                    <?php the_field('download_item_1_icon'); ?>
                    <h3><?php the_field('download_item_1_title'); ?></h3>
                  </a>
                </div>
                <div class="col col-12 col-lg-4 download-links-content-container">
                  <?php
                    $download_2_link_choice = get_field_object('download_item_2_link_choice');
                    $download_2_link_choice_value = $download_2_link_choice['value'];
                  ?>
                  <a class="download-card" href="<?php if($download_2_link_choice_value == 'Internal'): the_field('download_item_2_internal_link'); elseif ($download_2_link_choice_value == 'External'): the_field('download_item_2_external_link'); endif; ?>">
                    <?php the_field('download_item_2_icon'); ?>
                    <h3><?php the_field('download_item_2_title'); ?></h3>
                  </a>
                </div>
                <div class="col col-12 col-lg-4 download-links-content-container">
                  <?php
                    $download_3_link_choice = get_field_object('download_item_3_link_choice');
                    $download_3_link_choice_value = $download_3_link_choice['value'];
                  ?>
                  <a class="download-card" href="<?php if($download_3_link_choice_value == 'Internal'): the_field('download_item_3_internal_link'); elseif ($download_3_link_choice_value == 'External'): the_field('download_item_3_external_link'); endif; ?>">
                    <?php the_field('download_item_3_icon'); ?>
                    <h3><?php the_field('download_item_3_title'); ?></h3>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 page-container-content-container">
            <div class="content">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
    if (get_post_type() != 'careers') :
  ?>
  <section class="container-fluid main-container template-fullpageform-bottom">
    <div class="row">
      <div class="container">
        <div class="row justify-content-lg-end">
          <div class="col col-12 page-share-container">
            <?php echo crunchify_social_sharing_buttons(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
    else :
      get_template_part('template-parts/careers-features');
    endif;
    endwhile;
  else:
    echo '<p>Sorry, no content found</p>';
  endif;
get_footer();
?>
