<footer class="site-footer">
  <div class="container-fluid pre-footer">
    <div class="row pre-footer-cta-container">
      <a href="<?php echo get_option('footer_cta_1_url'); ?>" class="col col-12 col-lg-4 pre-footer-cta-item" style="background-image:url(<?php echo get_option('footer_cta_1_image'); ?>)">
        <img src="<?php echo get_option('footer_cta_1_image'); ?>" class="background-image" alt="">
        <h3><?php echo get_option('footer_cta_1_text'); ?></h3>
      </a>
      <a href="<?php echo get_option('footer_cta_2_url'); ?>" class="col col-12 col-lg-4 pre-footer-cta-item" style="background-image:url(<?php echo get_option('footer_cta_2_image'); ?>)">
        <img src="<?php echo get_option('footer_cta_2_image'); ?>" class="background-image" alt="">
        <h3><?php echo get_option('footer_cta_2_text'); ?></h3>
      </a>
      <a href="<?php echo get_option('footer_cta_3_url'); ?>" class="col col-12 col-lg-4 pre-footer-cta-item" style="background-image:url(<?php echo get_option('footer_cta_3_image'); ?>)">
        <img src="<?php echo get_option('footer_cta_3_image'); ?>" class="background-image" alt="">
        <h3><?php echo get_option('footer_cta_3_text'); ?></h3>
      </a>
    </div>
    <section class="row">
      <div class="container">
        <div class="row pre-footer-blog-title-container">
          <div class="col col-12 section-header-container">
            <div class="section-header">
			  <h1 class="pre-footer-blog-title-header-title">our audiologist says</h1>
              <div class="line"></div>
            </div>
          </div>
        </div>
        <div class="row pre-footer-blog-container">
          <?php
            $footer_posts_params = array('numberposts' => 4, 'category_name' => 'general',);
            $footer_recent_posts = wp_get_recent_posts($footer_posts_params);

            foreach( $footer_recent_posts as $footer_recent ){
              echo '<div class="col col-6 col-lg-3"><a href="' . get_permalink($footer_recent["ID"]) . '" class="pre-footer-blog-item-link">';
              echo '<article class="pre-footer-blog-item" aria-role="article"><div class="blog-thumbnail"><div class="overlay"></div>';
              echo get_the_post_thumbnail($footer_recent["ID"], 'footer-thumbnail') . '</div>';
			  echo '<p class="pre-footer-blog-item-link-date">' . get_the_time('F jS, Y', $footer_recent["ID"]) . '</p>';
			   echo '<h2 class="pre-footer-blog-item-link-title">' . get_the_title($footer_recent["ID"]) . '</h2>';
              echo '</article></a></div>';
            }

          get_footer();

          ?>
        </div>
      </div>
  </section>
  </div>
  <div class="container-fluid main-footer">
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col col-12 col-md-12 col-lg-4 main-footer-left-container">
            <div class="row">
              <div class="col col-12 col-sm-6 col-md-4 col-lg-12 main-footer-left-logo-container">
                <div class="branding">
                  <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/logo-white.svg" alt="Hearing Solutions home"/>
                  </a>
                </div>
              </div>
              <div class="col col-12 col-sm-6 col-md-4 col-lg-12 main-footer-left-contact-container">
                <div class="contact-info">
                  <ul id="contact">
                    <li>
                      <span>
                        call:
                      </span>
                      <a href="tel:<?php echo get_option('toll_free_phone_number'); ?>">
                        <?php echo get_option('toll_free_phone_number'); ?>
                      </a>
                    </li>
                    <li>
                      <span>
                        email:
                      </span>
                      <a href="mailto:<?php echo get_option('public_email'); ?>">
                        <?php echo get_option('public_email'); ?>
                      </a>
                    </li>
                  </ul>
                  <?php get_template_part('template-parts/social-icons'); ?>
                </div>
              </div>
              <div class="col col-12 col-sm-6 col-md-4 col-lg-12 main-footer-left-certificates-container">
                <?php get_template_part('template-parts/certificates-footer'); ?>
              </div>
            </div>
          </div>
          <div class="col col-12 col-md-12 col-lg-8 main-footer-right-container">
            <div class="row justify-content-end">
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-menu-container">
                <?php
                  $footer_1_nav_params = array(
                    'theme_location' => 'footer-navigation-1'
                  );
                  wp_nav_menu($footer_1_nav_params);
                ?>
              </div>
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-menu-container">
                <?php
                  $footer_2_nav_params = array(
                    'theme_location' => 'footer-navigation-2'
                  );
                  wp_nav_menu($footer_2_nav_params);
                ?>
              </div>
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-menu-container">
                <?php
                  $footer_3_nav_params = array(
                    'theme_location' => 'footer-navigation-3'
                  );
                  wp_nav_menu($footer_3_nav_params);
                ?>
              </div>
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-menu-container">
                <?php
                  $footer_4_nav_params = array(
                    'theme_location' => 'footer-navigation-4'
                  );
                  wp_nav_menu($footer_4_nav_params);
                ?>
              </div>
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-menu-container">
                <?php
                  $footer_5_nav_params = array(
                    'theme_location' => 'footer-navigation-5'
                  );
                  wp_nav_menu($footer_5_nav_params);
                ?>
              </div>
              <div class="col col-6 col-md-4 col-lg-3 main-footer-right-certificates-container">
                <?php get_template_part('template-parts/certificates-footer'); ?>
              </div>
              <div class="col col-md-12 col-lg-9 main-footer-right-location-menu-container">
                <?php
                  $footer_6_nav_params = array(
                    'theme_location' => 'footer-navigation-6'
                  );
                  wp_nav_menu($footer_6_nav_params);
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid copyright">
    <div class="row">
      <div class="col col-12 copyright-container">
         <p><a href="/privacy-policy">Privacy Policy</a> | Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
