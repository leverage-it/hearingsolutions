<?php

get_header();

if(have_posts()) :
  while (have_posts()) : the_post();

?>

<section class="container-fluid">
  <div class="row breadcrumb-container">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
</section>
<section class="container-fluid location-header-container">
  <div class="row">
    <div class="col col-12 col-lg-6 location-information-container">
      <div class="row justify-content-center">
        <div class="col col-lg-12 col-xl-9">
          <div class="row">
            <div class="col col-12 location-title-container">
              <h1>
                <?php
                  if (get_field('location_long_title')) :
                    the_field('location_long_title');
                  else :
                    the_title();
                  endif;
                ?>
              </h1>
            </div>
          </div>
          <div class="row">
            <div class="col col-12 location-details-container">
              <div class="info-item">
                <div class="info-item-container">
                  <h4>phone</h4>
                  <h3><a href="tel:<?php the_field('location_phone_number'); ?>"><?php the_field('location_phone_number'); ?></a></h3>
                </div>
                <i class="fal fa-phone"></i>
              </div>
              <div class="info-item">
                <div class="info-item-container">
                  <h4>fax</h4>
                  <p><?php the_field('location_fax_number'); ?></p>
                </div>
                <i class="fal fa-fax"></i>
              </div>
              <div class="info-item">
                <div class="info-item-container">
                  <h4>address</h4>
                  <div class="store-address">
                    <?php
                      $location = get_field('location_map');
                      if(isset($location['address'])) :
                          $address = substr($location['address'], 0, strrpos($location['address'], ','));
                          echo '<p>' . $address;
                      endif;
                      echo ', ';
                      the_field('location_province_state');
                      echo '<br/>';
                      the_field('location_country');
                      echo ', ';
                      the_field('location_postal_zip_code');
                      echo '</p>';

                      echo '<a class="button-blue" href="https://www.google.com/maps?saddr=My+Location&daddr=' . $location['lat'] . ',' . $location['lng'] . '" target="_blank">Get Directions</a>';
                    ?>
                  </div>
                </div>
                <i class="fal fa-map-marker-alt"></i>
              </div>
              <div class="info-item">
                <div class="info-item-container hours">
                  <h4>hours</h4>
                  <div class="store-hours">
                    <p>
                      Monday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_monday_hours') ):

                        	while( have_rows('location_monday_hours') ): the_row();

                        		$location_monday_opening_time = get_sub_field('location_monday_opening_time');
                        		$location_monday_closing_time = get_sub_field('location_monday_closing_time');

                            if ($location_monday_opening_time == null) {
                              echo '<i>' . $location_monday_closing_time . '</i>';
                            } elseif ($location_monday_closing_time == null) {
                              echo '<i>' . $location_monday_opening_time . '</i>';
                            } else {
                              echo $location_monday_opening_time . " - " . $location_monday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Tuesday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_tuesday_hours') ):

                        	while( have_rows('location_tuesday_hours') ): the_row();

                        		$location_tuesday_opening_time = get_sub_field('location_tuesday_opening_time');
                        		$location_tuesday_closing_time = get_sub_field('location_tuesday_closing_time');

                            if ($location_tuesday_opening_time == null) {
                              echo '<i>' . $location_tuesday_closing_time . '</i>';
                            } elseif ($location_tuesday_closing_time == null) {
                              echo '<i>' . $location_tuesday_opening_time . '</i>';
                            } else {
                              echo $location_tuesday_opening_time . " - " . $location_tuesday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Wednesday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_wednesday_hours') ):

                        	while( have_rows('location_wednesday_hours') ): the_row();

                        		$location_wednesday_opening_time = get_sub_field('location_wednesday_opening_time');
                        		$location_wednesday_closing_time = get_sub_field('location_wednesday_closing_time');

                            if ($location_wednesday_opening_time == null) {
                              echo '<i>' . $location_wednesday_closing_time . '</i>';
                            } elseif ($location_wednesday_closing_time == null) {
                              echo '<i>' . $location_wednesday_opening_time . '</i>';
                            } else {
                              echo $location_wednesday_opening_time . " - " . $location_wednesday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Thursday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_thursday_hours') ):

                        	while( have_rows('location_thursday_hours') ): the_row();

                        		$location_thursday_opening_time = get_sub_field('location_thursday_opening_time');
                        		$location_thursday_closing_time = get_sub_field('location_thursday_closing_time');

                            if ($location_thursday_opening_time == null) {
                              echo '<i>' . $location_thursday_closing_time . '</i>';
                            } elseif ($location_thursday_closing_time == null) {
                              echo '<i>' . $location_thursday_opening_time . '</i>';
                            } else {
                              echo $location_thursday_opening_time . " - " . $location_thursday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Friday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_friday_hours') ):

                        	while( have_rows('location_friday_hours') ): the_row();

                        		$location_friday_opening_time = get_sub_field('location_friday_opening_time');
                        		$location_friday_closing_time = get_sub_field('location_friday_closing_time');

                            if ($location_friday_opening_time == null) {
                              echo '<i>' . $location_friday_closing_time . '</i>';
                            } elseif ($location_friday_closing_time == null) {
                              echo '<i>' . $location_friday_opening_time . '</i>';
                            } else {
                              echo $location_friday_opening_time . " - " . $location_friday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Saturday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_saturday_hours') ):

                        	while( have_rows('location_saturday_hours') ): the_row();

                        		$location_saturday_opening_time = get_sub_field('location_saturday_opening_time');
                        		$location_saturday_closing_time = get_sub_field('location_saturday_closing_time');

                            if ($location_saturday_opening_time == null) {
                              echo '<i>' . $location_saturday_closing_time . '</i>';
                            } elseif ($location_saturday_closing_time == null) {
                              echo '<i>' . $location_saturday_opening_time . '</i>';
                            } else {
                              echo $location_saturday_opening_time . " - " . $location_saturday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                  <div class="store-hours">
                    <p>
                      Sunday
                    </p>
                    <p>
                      <?php
                        if( have_rows('location_sunday_hours') ):

                        	while( have_rows('location_sunday_hours') ): the_row();

                        		$location_sunday_opening_time = get_sub_field('location_sunday_opening_time');
                        		$location_sunday_closing_time = get_sub_field('location_sunday_closing_time');

                            if ($location_sunday_opening_time == null) {
                              echo '<i>' . $location_sunday_closing_time . '</i>';
                            } elseif ($location_sunday_closing_time == null) {
                              echo '<i>' . $location_sunday_opening_time . '</i>';
                            } else {
                              echo $location_sunday_opening_time . " - " . $location_sunday_closing_time;
                            }

                        	endwhile;
                         endif;
                        ?>
                    </p>
                  </div>
                </div>
                <i class="fal fa-calendar-alt"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col col-12 col-lg-6 location-map-container">
      <?php
        if( !empty($location) ):
      ?>
      <div class="acf-map">
      	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<section class="container-fluid main-container">
  <div class="row">
    <div class="container content-container">
      <div class="row">
        <div class="col col-12 page-content-container">
          <div class="content">
            <div class="row">
              <div class="col col-12 col-lg-6 location-left-container">
                <div class="location-left-content-container">
                  <div class="location-contact-form">
                    <h3>Book an appointment at this clinic</h3>
                    <a href="<?php echo get_post_type_archive_link('locations'); ?>">looking for another clinic?</a>
                    <?php gravity_form('5', false, false, false, true, true, '', true); ?>
                  </div>
                  <?php if(have_rows('location_image_slider')): ?>
                  <div class="location-gallery">
                    <div class="row">
                      <div class="col col-12">
                        <div class="gallery-title">
                          <h3>Photos</h3>
                        </div>
                      </div>
                    </div>
                    <div class="container">
                      <div class="row">
                      <?php while(have_rows('location_image_slider')) : the_row(); ?>
                        <a href="<?php the_sub_field('location_image'); ?>" class="col-6 col-lg-4 gallery-image wplightbox" data-group="gallery0">
                          <img src="<?php the_sub_field('location_image'); ?>" alt="">
                        </a>
                      <?php endwhile; ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col col-12">
                        <div class="gallery-footer"></div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>
                  <div class="location-reviews">
                    <h3>Reviews</h3>
                    <div id="google-reviews" data-places-id="<?php the_field('location_google_places_id')?>">
                    </div>
                  </div>
                  <div class="location-adzone">
                    <?php echo do_shortcode('[wpproads id="' . get_field('location_adzone_id') . '"]'); ?>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-lg-6 location-content-container">
                <?php the_content(); ?>
              </div>
            </div>
            <?php if(have_rows('location_staff_members')): ?>
            <div class="row">
              <?php while(have_rows('location_staff_members')): the_row(); ?>
              <div class="col col-12 col-md-6 col-lg-3 location-staff-member-container">
                <div class="staff-member">
                  <div class="image" style="background-image:url(<?php echo get_sub_field('location_staff_member_photo'); ?>);"></div>
                  <h4><?php echo get_sub_field('location_staff_member_name'); ?></h4>
                  <h5><?php echo get_sub_field('location_staff_member_title'); ?></h5>
                </div>
              </div>
            <?php endwhile; ?>
            </div>
          <?php endif; ?>
          </div>
        </div>
        <div class="col col-12 page-share-container">
          <?php echo crunchify_social_sharing_buttons(); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php

endwhile;

else:
echo '<p>Sorry, no content found</p>';

endif;

get_footer();
 ?>
