<?php

  require get_template_directory() . '/inc/function-admin.php';
  require get_template_directory() . '/inc/function-themesetup.php';
  require get_template_directory() . '/inc/function-excerpt.php';
  require get_template_directory() . '/inc/function-cptinmenu.php';
  require get_template_directory() . '/inc/function-breadcrumb.php';
  require get_template_directory() . '/inc/function-menuwidget.php';
  //require get_template_directory() . '/inc/function-announcementbar.php';
  require get_template_directory() . '/inc/function-socialshare.php';
  require get_template_directory() . '/inc/function-index.php';
  require get_template_directory() . '/inc/function-acf-googlemaps.php';
  require get_template_directory() . '/inc/function-locations.php';
  require get_template_directory() . '/inc/function-products.php';
  require get_template_directory() . '/inc/function-gravityforms.php';
  require get_template_directory() . '/inc/function-careers.php';
  require get_template_directory() . '/inc/function-slickslider.php';
  require get_template_directory() . '/inc/function-resources.php';
  require get_template_directory() . '/inc/function-widgets.php';
