<?php

  get_header();

?>

<section class="container-fluid">
  <div class="row breadcrumb-container">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
</section>
<section class="container-fluid careers-main-container">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3 page-sidebar-container">
          <nav class="sidebar-nav">
            <?php
              $page_sidebar_nav_params = array(
                'theme_location' => 'page-sidebar-navigation'
              );
              wp_nav_menu($page_sidebar_nav_params);
            ?>
          </nav>
        </div>
        <div class="col col-12 col-lg-9 page-container-content-container">
          <div class="content">
            <?php
            if(have_posts()) :
              while (have_posts()) : the_post();
              ?>
                <div class="row">
                  <div class="col col-12 col-md-8 career-title-container">
                    <div class="row">
                      <div class="col col-12 title">
                        <h1>
                          <?php
                            if (strpos(get_the_title(), 'in')) {
                              echo str_replace("in ". get_field('career_city'), "", get_the_title());
                            } else {
                              the_title();
                            }
                          ?>
                        </h1>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col col-12 subtitle">
                        <h4><?php the_field('career_city'); ?></h4>
                      </div>
                    </div>
                  </div>
                  <div class="col col-12 col-md-4 submit-container">
                    <div class="row">
                      <div class="col col-12 submit">
                        <a href="/careers/apply?jobposting-select=<?php echo get_the_title(); ?>" class="button-green">Submit resume</a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col col-12 linkedin">
                        <?php echo crunchify_social_sharing_buttons(['linkedin'], false) ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col col-12 career-content-container">
                    <?php the_content(); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col col-12 career-content-bottom-container">
                    <div class="row">
                      <div class="col col-12 bottom-message">
                        <h3>Join Hearing Solutions' team of dedicated hearing healthcare professionals and Apply Today!</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col col-12 bottom-submit">
                        <a href="/careers/apply?jobposting-select=<?php echo get_the_title(); ?>" class="button-green">Submit resume</a>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
              endwhile;
            else:
              echo '<p>Sorry, no content found</p>';
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_template_part('template-parts/careers-features'); ?>
<?php
get_footer();
?>
