<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <title><?php wp_title(); ?></title>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="loader" id="page-loader">
    <div class="spinner">

    </div>
  </div>
  <header class="site-header">
    <div class="container desktop-menu-container">
      <div class="row justify-content-end">
        <div class="col col-12 top-nav-container">
          <div class="top-nav" aria-role="top menu">
              <?php dynamic_sidebar('top-bar-widget-area'); ?>
              <?php get_template_part('template-parts/social-icons'); ?>
              <nav class="quick-nav">
                <?php
                  $top_nav_params = array(
                    'theme_location' => 'top-navigation'
                  );
                  wp_nav_menu($top_nav_params);
                ?>
              </nav>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-6 col-md-4 branding-container">
          <div class="branding">
            <a href="<?php echo home_url(); ?>">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/logo.svg" alt="Hearing Solutions home"/>
            </a>
          </div>
        </div>
        <div class="col col-6 col-md-8 middle-container">
          <div class="search-section">
            <div class="search-form-container">
              <?php get_search_form(); ?>
            </div>
            <button id="search-icon" type="button" aria-label="Search the website">
              <i class="fal fa-times"></i>
              <i class="far fa-search"></i>
            </button>
          </div>
        </div>
        <div class="col col-6 col-md-8 col-lg-12 main-nav-container">
          <nav class="nav" aria-label="Main menu">
            <?php
              $main_nav_params = array(
                'theme_location' => 'main-navigation'
              );
              wp_nav_menu($main_nav_params);
            ?>
          </nav>
          <nav class="cta" aria-label="Call to action menu">
            <?php
              $cta_nav_params = array(
                'theme_location' => 'cta-navigation'
              );
              wp_nav_menu($cta_nav_params);
            ?>
          </nav>
          <div class="mobile-menu-button-section">
            <button id="mobile-menu-button" type="button" aria-label="Toggle mobile menu">
              <i class="fal fa-bars"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mobile-menu-container" id="mobile-menu">
      <div class="container">
        <div class="row">
          <div class="col col-12 mobile-social-container">
            <?php get_template_part('template-parts/social-icons'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 mobile-quick-nav-container">
            <nav class="quick-nav" aria-label="Quick mobile navigation">
              <?php
                $top_nav_params = array(
                  'theme_location' => 'top-navigation'
                );
                wp_nav_menu($top_nav_params);
              ?>
            </nav>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 mobile-main-nav-container">
            <nav class="nav" aria-label="Main mobile navigation">
              <?php
                $main_nav_params = array(
                  'theme_location' => 'main-navigation'
                );
                wp_nav_menu($main_nav_params);
              ?>
            </nav>
            <nav class="cta" aria-label="Call to action mobile navigation">
              <?php
                $cta_nav_params = array(
                  'theme_location' => 'cta-navigation'
                );
                wp_nav_menu($cta_nav_params);
              ?>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
