<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
	<label for="s" class="visually-hidden">Search query</label>
  <input type="text" id="s" value="" name="s" placeholder="Search something">
  <button type="submit" id="searchsubmit"><i class="ion-md-search"></i> Search</button>
</form>
