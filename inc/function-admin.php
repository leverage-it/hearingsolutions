<?php

/*Admin Page*/

function hearingsolutions_add_admin_page() {
  //Generate Hearing Solutions Admin Page
  add_menu_page('Hearing Solutions Theme Options', 'Theme Options', 'manage_options', 'hearingsolutions_theme_options', 'hearingsolutions_theme_create_page', '' , 1000);
  //second last option is the custom icon, replace later, 20x20 pixels, png or svg
  //get_template_directory_uri . '/src'

  //Activate custom settings
  add_action('admin_init', 'hearingsolutions_custom_settings');
}

add_action('admin_menu', 'hearingsolutions_add_admin_page');

function hearingsolutions_custom_settings() {
  //register_setting('hearingsolutions-settings-group', 'announcement');
  register_setting('hearingsolutions-settings-group', 'facebook_handler');
  register_setting('hearingsolutions-settings-group', 'twitter_handler');
  register_setting('hearingsolutions-settings-group', 'linkedin_handler');
  register_setting('hearingsolutions-settings-group', 'youtube_handler');
  register_setting('hearingsolutions-settings-group', 'instagram_handler');
  register_setting('hearingsolutions-settings-group', 'toll_free_phone_number');
  register_setting('hearingsolutions-settings-group', 'public_email');
  register_setting('hearingsolutions-settings-group', 'footer_cta_1_text');
  register_setting('hearingsolutions-settings-group', 'footer_cta_1_url');
  register_setting('hearingsolutions-settings-group', 'footer_cta_1_image');
  register_setting('hearingsolutions-settings-group', 'footer_cta_2_text');
  register_setting('hearingsolutions-settings-group', 'footer_cta_2_url');
  register_setting('hearingsolutions-settings-group', 'footer_cta_2_image');
  register_setting('hearingsolutions-settings-group', 'footer_cta_3_text');
  register_setting('hearingsolutions-settings-group', 'footer_cta_3_url');
  register_setting('hearingsolutions-settings-group', 'footer_cta_3_image');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_image');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_1_title');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_2_title');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_3_title');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_1_text');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_2_text');
  register_setting('hearingsolutions-settings-group', 'careers_benefit_3_text');
  //add_settings_section('hearingsolutions-announcement-settings', 'Announcement', 'hearingsolutions_announcement_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-social-settings', 'Social Settings', 'hearingsolutions_social_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-footer-contact-settings', 'Footer Contact Settings', 'hearingsolutions_footer_contact_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-footer-cta-1-settings', 'Footer Call to Action 1 Settings', 'hearingsolutions_footer_cta_1_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-footer-cta-2-settings', 'Footer Call to Action 2 Settings', 'hearingsolutions_footer_cta_2_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-footer-cta-3-settings', 'Footer Call to Action 3 Settings', 'hearingsolutions_footer_cta_3_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-careers-benefits-0-settings', 'Career Benefits Section', 'hearingsolutions_careers_benefits_0_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-careers-benefits-1-settings', 'Career Benefits 1 Section', 'hearingsolutions_careers_benefits_1_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-careers-benefits-2-settings', 'Career Benefits 2 Section', 'hearingsolutions_careers_benefits_2_settings', 'hearingsolutions_theme_options');
  add_settings_section('hearingsolutions-careers-benefits-3-settings', 'Career Benefits 3 Section', 'hearingsolutions_careers_benefits_3_settings', 'hearingsolutions_theme_options');
  //add_settings_field('announcement', 'Announcement Message', 'hearingsolutions_announcement_message', 'hearingsolutions_theme_options', 'hearingsolutions-announcement-settings');
  add_settings_field('facebook_handler', 'Facebook', 'hearingsolutions_social_facebook', 'hearingsolutions_theme_options', 'hearingsolutions-social-settings');
  add_settings_field('twitter_handler', 'Twitter', 'hearingsolutions_social_twitter', 'hearingsolutions_theme_options', 'hearingsolutions-social-settings');
  add_settings_field('linkedin_handler', 'LinkedIn', 'hearingsolutions_social_linkedin', 'hearingsolutions_theme_options', 'hearingsolutions-social-settings');
  add_settings_field('youtube_handler', 'YouTube', 'hearingsolutions_social_youtube', 'hearingsolutions_theme_options', 'hearingsolutions-social-settings');
  add_settings_field('instagram_handler', 'Instagram', 'hearingsolutions_social_instagram', 'hearingsolutions_theme_options', 'hearingsolutions-social-settings');
  add_settings_field('toll_free_phone_number', 'Phone Number', 'hearingsolutions_toll_free_phone_number', 'hearingsolutions_theme_options', 'hearingsolutions-footer-contact-settings');
  add_settings_field('public_email', 'Email', 'hearingsolutions_public_email', 'hearingsolutions_theme_options', 'hearingsolutions-footer-contact-settings');
  add_settings_field('footer_cta_1_text', 'Call to Action Text', 'hearingsolutions_footer_cta_1_text', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-1-settings');
  add_settings_field('footer_cta_1_url', 'Call to Action URL', 'hearingsolutions_footer_cta_1_url', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-1-settings');
  add_settings_field('footer_cta_1_image', 'Call to Action Background Image', 'hearingsolutions_footer_cta_1_image', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-1-settings');
  add_settings_field('footer_cta_2_text', 'Call to Action Text', 'hearingsolutions_footer_cta_2_text', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-2-settings');
  add_settings_field('footer_cta_2_url', 'Call to Action URL', 'hearingsolutions_footer_cta_2_url', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-2-settings');
  add_settings_field('footer_cta_2_image', 'Call to Action Background Image', 'hearingsolutions_footer_cta_2_image', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-2-settings');
  add_settings_field('footer_cta_3_text', 'Call to Action Text', 'hearingsolutions_footer_cta_3_text', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-3-settings');
  add_settings_field('footer_cta_3_url', 'Call to Action Text', 'hearingsolutions_footer_cta_3_url', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-3-settings');
  add_settings_field('footer_cta_3_image', 'Call to Action Background Image', 'hearingsolutions_footer_cta_3_image', 'hearingsolutions_theme_options', 'hearingsolutions-footer-cta-3-settings');
  add_settings_field('careers_benefit_image', 'Benefits Background Image', 'hearingsolutions_careers_benefit_image', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-0-settings');
  add_settings_field('careers_benefit_1_title', 'Benefit 1 Title', 'hearingsolutions_careers_benefit_1_title', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-1-settings');
  add_settings_field('careers_benefit_2_title', 'Benefit 2 Title', 'hearingsolutions_careers_benefit_2_title', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-2-settings');
  add_settings_field('careers_benefit_3_title', 'Benefit 3 Title', 'hearingsolutions_careers_benefit_3_title', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-3-settings');
  add_settings_field('careers_benefit_1_text', 'Benefit 1 Text', 'hearingsolutions_careers_benefit_1_text', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-1-settings');
  add_settings_field('careers_benefit_2_text', 'Benefit 2 Text', 'hearingsolutions_careers_benefit_2_text', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-2-settings');
  add_settings_field('careers_benefit_3_text', 'Benefit 3 Text', 'hearingsolutions_careers_benefit_3_text', 'hearingsolutions_theme_options', 'hearingsolutions-careers-benefits-3-settings');
}

// function hearingsolutions_announcement_settings() {
//   echo "If you have any public announcements, write them in the field below.";
// }

function hearingsolutions_social_settings() {
  echo "Customize your social media links." ;
}

function hearingsolutions_footer_contact_settings() {
  echo "Add your public contact information for the footer.";
}

function hearingsolutions_footer_cta_1_settings() {
  echo "First Footer Call to Action Settings";
}

function hearingsolutions_footer_cta_2_settings() {
  echo "Second Footer Call to Action Settings";
}

function hearingsolutions_footer_cta_3_settings() {
  echo "Third Footer Call to Action Settings";
}

function hearingsolutions_careers_benefits_0_settings() {
  echo "Upload the Background Image for the Careers Benefits Section";
}

function hearingsolutions_careers_benefits_1_settings() {
  echo "First Careers Benefits Section";
}

function hearingsolutions_careers_benefits_2_settings() {
  echo "Second Careers Benefits Section";
}

function hearingsolutions_careers_benefits_3_settings() {
  echo "Third Careers Benefits Section";
}

function hearingsolutions_announcement_message() {
  $announcement = esc_attr( get_option('announcement'));
  echo '<input type="text" name="announcement" value="' . $announcement . '" placeholder="Write your announcement!" />';
}

function hearingsolutions_social_facebook() {
  $facebook = esc_attr( get_option('facebook_handler'));
  echo '<input type="text" name="facebook_handler" value="' . $facebook . '" placeholder="Facebook URL" />';
}

function hearingsolutions_social_twitter() {
  $twitter = esc_attr( get_option('twitter_handler'));
  echo '<input type="text" name="twitter_handler" value="' . $twitter . '" placeholder="Twitter URL" />';
}

function hearingsolutions_social_linkedin() {
  $linkedin = esc_attr( get_option('linkedin_handler'));
  echo '<input type="text" name="linkedin_handler" value="' . $linkedin . '" placeholder="LinkedIn URL" />';
}

function hearingsolutions_social_youtube() {
  $youtube = esc_attr( get_option('youtube_handler'));
  echo '<input type="text" name="youtube_handler" value="' . $youtube . '" placeholder="YouTube URL" />';
}

function hearingsolutions_social_instagram() {
  $instagram = esc_attr( get_option('instagram_handler'));
  echo '<input type="text" name="instagram_handler" value="' . $instagram . '" placeholder="Instagram URL" />';
}

function hearingsolutions_toll_free_phone_number() {
  $phone = esc_attr( get_option('toll_free_phone_number'));
  echo '<input type="tel" name="toll_free_phone_number" value="' . $phone . '" placeholder="Toll Free Number" />';
}

function hearingsolutions_public_email() {
  $email = esc_attr( get_option('public_email'));
  echo '<input type="email" name="public_email" value="' . $email . '" placeholder="Email" />';
}

function hearingsolutions_footer_cta_1_text() {
  $cta_1_text = esc_attr( get_option('footer_cta_1_text'));
  echo '<input type="text" name="footer_cta_1_text" value="' . $cta_1_text . '" placeholder="Call to Action Text" />';
}

function hearingsolutions_footer_cta_1_url() {
  $cta_1_url = esc_attr( get_option('footer_cta_1_url'));
  echo '<input type="text" name="footer_cta_1_url" value="' . $cta_1_url . '" placeholder="Call to Action URL" />';
}

function hearingsolutions_footer_cta_1_image() {
  $background_image = esc_attr(get_option('footer_cta_1_image'));
  echo '<input type="button" value="Upload Background Image" id="footer-cta-1-bgimage-button"/><input type="hidden" name="footer_cta_1_image" value="' . $background_image . '" id="footer-cta-1-bgimage" /><div id="footer-cta-3-bgimage-preview" class="selected-bgimage"><img src="' . $background_image . '"  alt=""/></div>';

}

function hearingsolutions_footer_cta_2_text() {
  $cta_2_text = esc_attr( get_option('footer_cta_2_text'));
  echo '<input type="text" name="footer_cta_2_text" value="' . $cta_2_text . '" placeholder="Call to Action Text" />';
}

function hearingsolutions_footer_cta_2_url() {
  $cta_2_url = esc_attr( get_option('footer_cta_2_url'));
  echo '<input type="text" name="footer_cta_2_url" value="' . $cta_2_url . '" placeholder="Call to Action URL" />';
}

function hearingsolutions_footer_cta_2_image() {
  $background_image = esc_attr(get_option('footer_cta_2_image'));
  echo '<input type="button" value="Upload Background Image" id="footer-cta-2-bgimage-button"/><input type="hidden" name="footer_cta_2_image" value="' . $background_image . '" id="footer-cta-2-bgimage" /><div id="footer-cta-3-bgimage-preview" class="selected-bgimage"><img src="' . $background_image . '"  alt=""/></div>';

}

function hearingsolutions_footer_cta_3_text() {
  $cta_3_text = esc_attr( get_option('footer_cta_3_text'));
  echo '<input type="text" name="footer_cta_3_text" value="' . $cta_3_text . '" placeholder="Call to Action Text" />';
}

function hearingsolutions_footer_cta_3_url() {
  $cta_3_url = esc_attr( get_option('footer_cta_3_url'));
  echo '<input type="text" name="footer_cta_3_url" value="' . $cta_3_url . '" placeholder="Call to Action URL" />';
}

function hearingsolutions_footer_cta_3_image() {
  $background_image = esc_attr(get_option('footer_cta_3_image'));
  echo '<input type="button" value="Upload Background Image" id="footer-cta-3-bgimage-button"/><input type="hidden" name="footer_cta_3_image" value="' . $background_image . '" id="footer-cta-3-bgimage" /><div id="footer-cta-3-bgimage-preview" class="selected-bgimage"><img src="' . $background_image . '"  alt=""/></div>';

}

function hearingsolutions_careers_benefit_image() {
  $background_image = esc_attr(get_option('careers_benefit_image'));
  echo '<input type="button" value="Upload Background Image" id="careers-benefit-bgimage-button"/><input type="hidden" name="careers_benefit_image" value="' . $background_image . '" id="careers-benefit-bgimage" /><div id="careers-benefits-bgimage-preview" class="selected-bgimage"><img src="' . $background_image . '"  alt=""/></div>';

}

function hearingsolutions_careers_benefit_1_title() {
  $benefit_1_title = esc_attr( get_option('careers_benefit_1_title'));
  echo '<input type="text" name="careers_benefit_1_title" value="' . $benefit_1_title . '" placeholder="Benefits Title" maxlength="16" />';
}

function hearingsolutions_careers_benefit_2_title() {
  $benefit_2_title = esc_attr( get_option('careers_benefit_2_title'));
  echo '<input type="text" name="careers_benefit_2_title" value="' . $benefit_2_title . '" placeholder="Benefits Title" maxlength="16" />';
}

function hearingsolutions_careers_benefit_3_title() {
  $benefit_3_title = esc_attr( get_option('careers_benefit_3_title'));
  echo '<input type="text" name="careers_benefit_3_title" value="' . $benefit_3_title . '" placeholder="Benefits Title"  maxlength="16" />';
}

function hearingsolutions_careers_benefit_1_text() {
  $benefit_1_text = esc_attr( get_option('careers_benefit_1_text'));
  echo '<input type="text" name="careers_benefit_1_text" value="' . $benefit_1_text . '" maxlength="120" />';
}

function hearingsolutions_careers_benefit_2_text() {
  $benefit_2_text = esc_attr( get_option('careers_benefit_2_text'));
  echo '<input type="text" name="careers_benefit_2_text" value="' . $benefit_2_text . '" maxlength="120" />';
}

function hearingsolutions_careers_benefit_3_text() {
  $benefit_3_text = esc_attr( get_option('careers_benefit_3_text'));
  echo '<input type="text" name="careers_benefit_3_text" value="' . $benefit_3_text . '" maxlength="120" />';
}

function hearingsolutions_theme_create_page() {
  require_once(get_template_directory() . '/inc/templates/hearingsolutions-admin.php');
}
