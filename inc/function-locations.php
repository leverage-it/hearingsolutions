<?php

function locations_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'locations' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
            $query->set( 'orderby', 'title');
            $query->set( 'order', 'ASC');
    }
}
add_action( 'pre_get_posts', 'locations_query' );
