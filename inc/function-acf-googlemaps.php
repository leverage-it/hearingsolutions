<?php

//Initializing Google Maps
function my_acf_init() {
  acf_update_setting('google_api_key', 'AIzaSyDXD-GZXg2NnQZS5m9jtF-kRNBv11XaHGQ');
}

add_action('acf/init', 'my_acf_init');

//Draws Google Map on Location Pages
function map(){
  $theme_attributes = array('url' => get_stylesheet_directory_uri());
  wp_register_script('map', get_template_directory_uri() . '/js/map.js', array(), 1, true);
  if(get_post_type()=='locations' ){
    wp_enqueue_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDXD-GZXg2NnQZS5m9jtF-kRNBv11XaHGQ&libraries=places', array(), 1, true);
    wp_enqueue_script('map');
    wp_localize_script('map', 'themeAttributes', $theme_attributes);
  }
}
add_action('wp_enqueue_scripts', 'map', 20);
