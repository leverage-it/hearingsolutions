<?php
  function hearingsolutions_widgets() {
    register_sidebar( array(
      'name' => 'Top Bar Widget Area',
      'id' => 'top-bar-widget-area'
    ));
  }

  add_action('widgets_init', 'hearingsolutions_widgets');
