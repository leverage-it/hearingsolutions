<?php

function index_query( $query ){
  if ($query->is_main_query() && $query-> is_home()) {
      $query->set( 'offset', '1');
      $offset = 1;
      $ppp = get_option('posts_per_page');
      if ( $query->is_paged ) {
          $page_offset = $offset + ( ($query->query_vars['paged']-1) * $ppp );
          $query->set('offset', $page_offset );

      }
      else {
          $query->set('offset',$offset);
      }
  }
}
add_action( 'pre_get_posts', 'index_query', 1);
