<?php

function hearingsolutions_resources() {
  wp_enqueue_style('hearingsolutions-style', get_stylesheet_uri());
  wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
  wp_enqueue_style('fontawesome', 'https://pro.fontawesome.com/releases/v5.0.13/css/all.css');
  wp_enqueue_script('critical-styles', get_template_directory_uri() . '/js/critical.js', array() , 1, false);
  wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array() , 1, true);
  wp_enqueue_script('app', get_template_directory_uri() . '/js/app.js', array() , 1, true);
  wp_enqueue_style('styles', get_template_directory_uri() . '/css/styles.css');
}

add_action('wp_enqueue_scripts', 'hearingsolutions_resources');

function hearingsolutions_admin_resources($hook) {
  if ('toplevel_page_hearingsolutions_theme_options' != $hook) {
    return;
  }
  wp_register_script('hearingsolutions-admin-js', get_template_directory_uri() . '/js/admin.js', array('jquery'), '1.0', true);

  wp_enqueue_media();
  wp_enqueue_style('hearingsolutions-admin-css', get_template_directory_uri() . '/css/styles-admin.css');
  wp_enqueue_script('hearingsolutions-admin-js');

}

add_action('admin_enqueue_scripts' , 'hearingsolutions_admin_resources');
