<?php

if ( !is_customize_preview() ) {
  /* take care of the urls */
  add_filter( 'wp_get_nav_menu_items', 'services_menu_filter', 12, 3 );
  function services_menu_filter( $items, $menu, $args ) {
      /* alter the URL for cpt-archive objects */

      $menu_order = count($items); /* Offset menu order */
      $child_items = array();

      foreach ( $items as &$item ) {
          if ( $item->title != '##careers##' ) continue;
          $item->url = '/careers/';
          $item->title = 'Careers';

          foreach ( get_posts( 'post_type=careers&numberposts=-1' ) as $post ) {
            if ($post->post_title != 'Apply') {
              $post->menu_item_parent = $item->ID;
              $post->post_type = 'nav_menu_item';
              $post->object = 'custom';
              $post->type = 'custom';
              $post->menu_order = ++$menu_order;
              $post->title = $post->post_title;
              $post->url = get_permalink( $post->ID );
              /* add children */
              $child_items []= $post;
            }
          }
      }
      return array_merge( $items, $child_items );
  }
}
