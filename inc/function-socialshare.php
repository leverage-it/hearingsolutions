<?php

//Social Media Share Buttons
function crunchify_social_sharing_buttons($social_services = [''], $label = true) {
	global $post;
	if(is_singular() || is_home()){

		// Get current page URL
		$crunchifyURL = urlencode(get_permalink());

		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());

		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$whatsappURL = 'whatsapp://send?text='.$crunchifyTitle . ' ' . $crunchifyURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$crunchifyURL.'&amp;title='.$crunchifyTitle;

		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;

		$social_services_length = count($social_services);

		// Add sharing button at the end of page/page content
		$content .= '<div class="crunchify-social">';

		if ($label == true) {
			$content .= '<p>Share this resource</p>';
		}

		$add_facebook = function() use ($facebookURL, $crunchifyURL) {
			return '<a class="crunchify-link crunchify-facebook" href="'.$facebookURL.'" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a>';
		};
		$add_twitter = function() use ($twitterURL, $crunchifyTitle, $crunchifyURL) {
			return '<a class="crunchify-link crunchify-twitter" href="'. $twitterURL .'" target="_blank"><i class="fab fa-twitter"></i> Twitter</a>';
		};
		$add_whatsapp = function() use ($whatsappURL, $crunchifyTitle, $crunchifyURL) {
			return '<a class="crunchify-link crunchify-whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="fab fa-whatsapp"></i> WhatsApp</a>';
		};
		$add_googleplus = function() use ($googleURL, $crunchifyURL) {
			return '<a class="crunchify-link crunchify-googleplus" href="'.$googleURL.'" target="_blank"><i class="fab fa-google-plus-g"></i> Google+</a>';
		};
		$add_linkedin = function() use ($linkedInURL, $crunchifyURL, $crunchifyTitle) {
			return '<a class="crunchify-link crunchify-linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fab fa-linkedin-in"></i> Share</a>';
		};
		$add_pinterest = function() use ($pinterestURL, $crunchifyURL, $crunchifyTitle, $crunchifyThumbnail) {
			return '<a class="crunchify-link crunchify-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><i class="fab fa-pinterest-p"></i> Pin It</a>';
		};

		for ($x = 0; $x < $social_services_length; $x++) {
			if ($social_services[$x] != 'all') {
				if($social_services[$x] == 'facebook') {
					$content .= $add_facebook();
				} elseif ($social_services[$x] == 'twitter') {
					$content .= $add_twitter();
				} elseif ($social_services[$x] == 'whatsapp') {
					$content .= $add_whatsapp();
				} elseif ($social_services[$x] == 'googleplus') {
					$content .= $add_googleplus();
				} elseif ($social_services[$x] == 'linkedin') {
					$content .= $add_linkedin();
				} elseif ($social_services[$x] == 'pinterest') {
					$content .= $add_pinterest();
				} else {
					$content .= $add_facebook();
					$content .= $add_twitter();
				}
			} else {
				$content .= $add_facebook();
				$content .= $add_twitter();
				$content .= $add_whatsapp();
				$content .= $add_googleplus();
				$content .= $add_linkedin();
				$content .= $add_pinterest();
			}
		}

		$content .= '</div>';

		return $content;
	}else{
		// if not a post/page then don't include sharing button
		return $content;
	}
}
