<?php

 function if_has_parent($post, $array) {
  if ($post->post_parent <= 0) {
    for ($i = 0; $i < count($array); $i++) {
      echo "<a href=". $array[$i]["url"] . " rel='nofollow'>" . $array[$i]["title"] . "</a>&nbsp;&nbsp;>&nbsp;&nbsp;";
    }
    return;
  } else {
    $parent_title = get_the_title($post->post_parent);
    $parent_url = get_page_link($post->post_parent);
    $post_info = array("title"=>$parent_title, "url"=>$parent_url);
    array_unshift($array, $post_info);
    if_has_parent(get_post($post->post_parent), $array);
  }
}

//Breadcrumb Function
  function get_breadcrumb() {
    echo '<div class="bread-crumb">';
    echo '<a href="'.home_url().'" rel="nofollow"><i class="fas fa-home"></i></a>';
    echo "&nbsp;&nbsp;>&nbsp;&nbsp;";
    if (is_single()) {
      if (get_post_type() == 'post') {
        echo '<a href="' . get_post_type_archive_link( 'post' ) . '" rel="nolfollow">Blog</a>';
      } elseif (get_post_type() == 'locations' || get_post_type() == 'careers') {
        echo '<a href="' . get_post_type_archive_link(get_post_type()) . '" rel="nofollow">' . get_post_type() . '</a>';
      } elseif (get_post_type() == 'products') {
        echo '<a href="/hearing-aid-brands">hearing aid brands</a>';
        echo "&nbsp;&nbsp;>&nbsp;&nbsp;";
        $post_object = get_post();
        $post_term_object = get_the_terms($post_term, 'brands');
        //var_dump($post_term_object);
        echo '<a href="'. get_term_link($post_term_object[0]) . '">'. $post_term_object[0]->name . '</a>';
      }
      echo "&nbsp;&nbsp;>&nbsp;&nbsp; <span class='bread-crumb-blog-title'>" . get_the_title() . "</span>";
    } elseif(is_archive()) {
      if (is_category()) {
        echo '<a href="' . get_post_type_archive_link( 'post' ) . '" rel="nolfollow">Blog</a>';
        echo "&nbsp;&nbsp;>&nbsp;&nbsp;";
        echo single_cat_title();
      } elseif(is_tax('brands')) {
        echo '<a href="/hearing-aid-brands">hearing aid brands</a>';
        echo "&nbsp;&nbsp;>&nbsp;&nbsp;";
        echo get_queried_object()->name;

      } else {
        echo str_replace("Archives: ", "", get_the_archive_title());
      }
    } elseif (is_home()) {
        echo "Blog";
    } elseif (is_page()) {
      global $post;
        $parent_pages = array();
        if_has_parent($post, $parent_pages);
        echo the_title();
    } elseif (is_search()) {
        echo "Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
    echo '</div>';
}
