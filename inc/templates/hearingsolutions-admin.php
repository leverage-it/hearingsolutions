<h1>Hearing Solutions Theme Options</h1>
<?php settings_errors(); ?>
<form method="post" action="options.php">
  <?php settings_fields('hearingsolutions-settings-group'); ?>
  <?php do_settings_sections('hearingsolutions_theme_options'); ?>
  <?php submit_button(); ?>
</form>
