<?php
function products_query( $query ){
    if( ! is_admin()
        && $query->is_tax( 'brands' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
            $query->set( 'orderby', 'title');
            $query->set( 'order', 'ASC');
    }
}
add_action( 'pre_get_posts', 'products_query' );
