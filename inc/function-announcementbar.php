<?php

 function announcement_bar_body_class($classes) {
   if(get_option('announcement') != null) {
     $classes[] = 'has-announcement';
     return $classes;
   }
 }

 add_filter('body_class', 'announcement_bar_body_class');
