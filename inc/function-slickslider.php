<?php

function slickslider() {
  wp_register_script('slickslider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), 1, true);
  if ((get_post_type() == 'careers' && is_archive()) || is_front_page() ) {
      wp_enqueue_script('slickslider');
      wp_enqueue_script('slick', get_template_directory_uri()  . '/js/slick.js', array(), 1, true);
  } elseif ((get_post_type() == 'locations' && is_single())) {
    wp_enqueue_script('slickslider');
    wp_enqueue_script('slick', get_template_directory_uri()  . '/js/slick.js', array('map'), 1, true);
  }
}

add_action('wp_enqueue_scripts', 'slickslider');
