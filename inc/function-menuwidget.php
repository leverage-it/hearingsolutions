<?php

class hearingsolutions_menu_widget extends WP_Widget {
  public function __construct() {
    $widget_options = array (
      'classname' => 'hearingsolutions-menu-widget',
      'description' => 'Custom Menu Widget',
    );
    parent::__construct('hearingsolutions-menu-widget', 'Menu Widget', $widget_options);
  }

  public function form ($instance) {
    echo '<p>Menu Widget Options</p>';
  }

  public function widget( $args, $instance ) {
      // outputs the content of the widget
      if ( ! isset( $args['widget_id'] ) ) {
        $args['widget_id'] = $this->id;
      }

      // widget ID with prefix for use in ACF API functions
      $widget_id = 'widget_' . $args['widget_id'];

      $title = get_field( 'menu_widget_title', $widget_id ) ? get_field( 'menu_widget_title', $widget_id ) : '';
      $btn_text = get_field( 'menu_widget_button_text', $widget_id );
      $btn_link = get_field( 'menu_widget_button_link', $widget_id ) ? get_field( 'menu_widget_button_link', $widget_id ) : '#';
      $btn_bgimage = get_field( 'menu_widget_background_image', $widget_id ) ? get_field( 'menu_widget_background_image', $widget_id ) : '#';

      echo $args['before_widget'];

      echo '<div class="menu-action" style="background-image: url(' . $btn_bgimage . ');">';

      if ( $title ) {
        echo $args['before_title'] . esc_html($title) . $args['after_title'];
      }
      the_field( 'text', $widget_id );

      if($btn_text) {
        echo '<a href="' . esc_url($btn_link) . '" class="button-yellow">' . $btn_text . '</a>';
      }

      echo '</div>';

      echo $args['after_widget'];

    }
}

add_action('widgets_init', function() {
    register_widget( 'hearingsolutions_menu_widget' );
});
