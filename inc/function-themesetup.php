<?php

//Theme Setup
function hearingsolutions_setup() {

  //Navigation Menus
  register_nav_menus(array(
    'top-navigation' => __('Top Navigation'),
    'main-navigation' => __('Main Navigation'),
    'cta-navigation' => __('Call to Action Navigation'),
    'page-sidebar-navigation' => __('Page Sidebar Navigation'),
    'footer-navigation-1' => __('Footer Navigation 1'),
    'footer-navigation-2' => __('Footer Navigation 2'),
    'footer-navigation-3' => __('Footer Navigation 3'),
    'footer-navigation-4' => __('Footer Navigation 4'),
    'footer-navigation-5' => __('Footer Navigation 5'),
    'footer-navigation-6' => __('Footer Navigation 6'),
  ));

  //Featured Image Support
  add_theme_support('post-thumbnails');
  add_image_size('footer-thumbnail', 300, 148, true);
  add_image_size('feed-thumbnail', 610, 300, true);
  add_image_size('single-featured-image', 800);
  add_image_size('feed-header-thumbnail', 3840);

  add_post_type_support( 'page', 'excerpt' );
}

add_action('after_setup_theme', 'hearingsolutions_setup');
