<?php

/**
 * Create ACF setting page under Careers CPT menu
 *
 * @since 1.0.0
 */
if ( function_exists( 'acf_add_options_page' ) ){
    acf_add_options_sub_page(array(
        'title'      => 'Careers Settings',
        'parent'     => 'edit.php?post_type=careers',
        'capability' => 'manage_options'
    ));
}
