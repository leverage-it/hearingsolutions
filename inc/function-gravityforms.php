<?php
add_filter( 'gform_field_value_location_select', 'location_select');

function location_select() {
  return get_the_title();
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

add_filter( 'gform_pre_render_8', 'populate_jobposting' );
add_filter( 'gform_pre_validation_8', 'populate_jobposting' );
add_filter( 'gform_pre_submission_filter_8', 'populate_jobposting' );
add_filter( 'gform_admin_pre_render_8', 'populate_jobposting' );

function populate_jobposting( $form ) {

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'jobposting-select' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'numberposts=-1&post_type=careers' );

        $choices = array();

        foreach ( $posts as $post ) {
          if ($post->post_title != "Apply") {
              $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
          }
        }

        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Post';
        $field->choices = $choices;

    }

    return $form;
}
