<div class="post-container">
<a class="thumbnail-link" href="<?php the_permalink(); ?>">
  <?php
    if(has_post_thumbnail()):
        the_post_thumbnail('feed-thumbnail');
    else:
  ?>
  <img src=""  alt="">
  <?php
    endif;
  ?>
</a>
<div class="post-container-content">
  <p class="post-container-meta">
    <?php the_time('F jS, Y'); ?> |
      by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> |
    <?php
      $categories = get_the_category();
      $categories_output = '';

      if($categories) {
        foreach($categories as $category) {
          $categories_output .= '<a class="post-categories" href="' . get_category_link($category->term_id) . '">' . $category->cat_name . '</a>';
        }
        echo $categories_output;
      }
      ?>
  </p>
  <h1 class="post-container-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
  <?php the_excerpt(); ?>
</div>
</div>
