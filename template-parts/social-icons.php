<ul id="social">
  <li>
    <a href="<?php echo get_option('facebook_handler'); ?>" target="_blank" aria-label="Hearing Solutions on Facebook">
      <i class="fab fa-facebook-f"></i>
    </a>
  </li>
  <li>
    <a href="<?php echo get_option('twitter_handler'); ?>" target="_blank" aria-label="Hearing Solutions on Twitter">
      <i class="fab fa-twitter"></i>
    </a>
  </li>
  <li>
    <a href="<?php echo get_option('linkedin_handler'); ?>" target="_blank" aria-label="Hearing Solutions on LinkedIn">
      <i class="fab fa-linkedin-in"></i>
    </a>
  </li>
  <li>
    <a href="<?php echo get_option('youtube_handler'); ?>" target="_blank" aria-label="Hearing Solutions on YouTube">
      <i class="fab fa-youtube"></i>
    </a>
  </li>
  <li>
    <a href="<?php echo get_option('instagram_handler'); ?>" target="_blank" aria-label="Hearing Solutions on Instagram">
      <i class="fab fa-instagram"></i>
    </a>
  </li>
</ul>
