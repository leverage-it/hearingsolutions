<div class="certificates">
  <div class="certificate">
    <a href="https://www.bbb.org/kitchener/business-reviews/hearing-assistive-devices/hearing-solutions-in-toronto-on-1276403#sealclick">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bbb.png" alt="Hearing Solutions is a BBB Accredited Business"/>
    </a>
  </div>
  <div class="certificate">
    <script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert"></script>
  </div>
  <div class="certificate">
    <a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=hearingsolutions.ca','SiteLock','width=600,height=600,left=160,top=170');" >
      <img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/hearingsolutions.ca" />
    </a>
  </div>
</div>
