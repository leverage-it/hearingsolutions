<section class="container-fluid career-features-section-container jarallax">
  <img src="<?php echo get_option('careers_benefit_image'); ?>" class="jarallax-img page-header-image" alt="">
  <div class="row">
    <div class="container careers-features-container">
      <div class="row">
        <div class="col col-12">
          <div class="section-header">
            <h3>Why Choose Us</h3>
            <div class="line"></div>
          </div>
        </div>
      </div>
      <div class="row benefits-container">
        <div class="col col-12 col-md-4 benefit-container">
          <div class="benefit">
            <h4><?php echo get_option('careers_benefit_1_title'); ?></h4>
            <p><?php echo get_option('careers_benefit_1_text'); ?></p>
          </div>
        </div>
        <div class="col col-12 col-md-4 benefit-container">
          <div class="benefit">
            <h4><?php echo get_option('careers_benefit_2_title'); ?></h4>
            <p><?php echo get_option('careers_benefit_2_text'); ?></p>
          </div>
        </div>
        <div class="col col-12 col-md-4 benefit-container">
          <div class="benefit">
            <h4><?php echo get_option('careers_benefit_3_title'); ?></h4>
            <p><?php echo get_option('careers_benefit_3_text'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
