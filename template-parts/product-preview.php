<div class="product-container">
  <a class="thumbnail-link" href-"<?php the_permalink(); ?>">
    <?php
      if(has_post_thumbnail()):
          the_post_thumbnail('feed-thumbnail');
      else:
    ?>
    <img src=""  alt="">
    <?php
      endif;
    ?>
  </a>
  <div class="product-container-content">
    <h1 class="product-container-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
  </div>
</div>
