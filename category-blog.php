<?php
  get_header();

  $blog_params = array(
    'category_name'=>'blog',
    'posts_per_page'=>'1'
  );

  $header_query = new WP_Query($blog_params);
  while($header_query->have_posts()) : $header_query->the_post();
?>

<section class="container-fluid blog-header-container jarallax">
  <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="jarallax-img page-header-image" alt="">
  <div class="row breadcrumb-container-transparent">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
  <div class="row blog-header-titles">
    <div class="container">
      <div class="row">
        <div class="col col-12 col-md-8">
          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 col-md-8">
          <p class="blog-header-titles-meta"><?php the_time('F jS, Y');?> by <a class="author-link" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
endwhile;
wp_reset_postdata();
?>
<section class="container-fluid main-container">
  <div class="row">
    <div class="container">
      <div class="row">
        <?php
        query_posts('category_name=blog&offset=1');

        if(have_posts()) :
        while (have_posts()) : the_post();
        ?>
          <div class="col col-12 col-lg-6">
            <?php get_template_part('template-parts/content-preview'); ?>
          </div>
        <?php endwhile;
          else:
            echo '<p>Sorry, no content found</p>';
          endif; ?>
      </div>
    </div>
  </div>
</section>
<?php
  get_footer();
?>
