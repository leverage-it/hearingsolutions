<?php

  get_header();

?>

<section class="container-fluid">
  <div class="row breadcrumb-container">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
</section>
<section class="container-fluid locations-map-container">
  <div class="row">
    <div class="container">
      <div class="row locations-map-search-container">
        <div class="col col-12">
          <div class="locations-overlay locations-searchform">
            <div class="row">
              <div class="col col-12 title-container">
                <h1>Find a location close to you</h1>
              </div>
            </div>
            <div class="row">
              <div class="col col-12 searchsection-container">
                <div class="locations-searchform-container">
                  <input id="locations-search" name="locations-search" type="text" placeholder="Find a location near you">
                </div>
                <i id="map-recenter" class="fas fa-crosshairs"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row locations-acf-map-container">
    <div class="acf-map">
      <?php
        if(have_posts()) :
        while (have_posts()) : the_post();
        $location = get_field('location_map');
      ?>
      <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
				<h4><?php the_title(); ?></h4>
				<p class="address"><?php echo $location['address']; ?></p>
			</div>
      <?php
        endwhile;
        else:
          echo '<p>Sorry, no content found</p>';
        endif;
      ?>
    </div>
  </div>
</section>
<section class="container-fluid locations-list-container">
  <div class="container">
    <div class="row">
      <div class="col col-12 locations-list-filter-container">
        <div class="locations-overlay locations-list-filter">
          <div class="row">
            <div class="col col-12">
              <div class="section-header">
                <h3>Our Locations</h3>
                <div class="line"></div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col col-12 col-sm-5 list-filter-select-container">
              <div class="select">
                <select id="filter-city">
                  <option selected="true" disabled="disabled">Filter by City</option>
                  <option>All Cities</option>
                  <?php
                  if(have_posts()) :

                    $location_cities = [];

                    while (have_posts()) : the_post();
                      $location_city_group = get_field('location_city_group');
                      if (!in_array($location_city_group, $location_cities)) {
                        $location_cities[] = $location_city_group;
                        sort($location_cities);
                      }
                    endwhile;

                    foreach ($location_cities as $city) {
                  ?>
                      <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                  <?php
                    }
                    else:
                      echo '<p>Sorry, no content found</p>';
                    endif;
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="container location-list-items-container">
        <div class="row">
          <?php

            if(have_posts()) :
            while (have_posts()) : the_post();

            $location_map = get_field('location_map');
            $location_city = get_field('location_city');
            $location_city_group = get_field('location_city_group');

            ?>

            <div class="col col-12 col-sm-6 col-md-4 col-lg-3 list-item-block-container" data-city-group="<?php echo $location_city_group; ?>">
              <div class="list-item-container">
                <div class="city-group">
                  <h5><?php echo $location_city_group; ?></h5>
                </div>
                <div class="list-item-content">
                  <a href="<?php echo get_post_permalink(); ?>">
                    <h3><?php the_title(); ?></h3>
                  </a>
                  <p><?php echo $location_map['address']; ?></p>
                  <div class="hours">
                    <div class="title">
                      Hours
                    </div>
                    <div class="info">
                      <?php
                        the_field('location_quick_hours');
                      ?>
                    </div>
                  </div>
                  <a class="get-direction" href="https://www.google.com/maps?saddr=My+Location&daddr=<?php echo $location_map['lat']; ?>,<?php echo $location_map['lng']; ?>" target="_blank">get directions</a>
                  <a class="button-blue" href="<?php echo get_post_permalink(); ?>">contact</a>
                </div>
              </div>
            </div>

            <?php endwhile;
            else:
              echo '<p>Sorry, no content found</p>';
            endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
?>
