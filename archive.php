<?php

get_header(); ?>


<section class="container-fluid blog-main-container">
  <div class="row">
    <div class="container">
      <div class="row breadcrumb-container">
        <?php get_template_part('template-parts/breadcrumb'); ?>
      </div>
      <div class="row">
        <div class="col col-12 archive-content-container">
          <div class="content">
            <div class="row">
              <div class="col col-12 archive-title-container">
                <h2>
                  <?php
                    if (is_category()) {
                      single_cat_title();
                    } elseif (is_tag()) {
                      single_tag_title();
                    } elseif (is_author()) {
                      the_post();
                      echo get_the_author() . ' Archives';
                      rewind_posts();
                    } elseif (is_day()) {
                      echo 'Daily Archives: ' . get_the_date();
                    } elseif (is_month()) {
                      echo 'Monthly Archives: ' . get_the_date('F Y');
                    } elseif (is_year()) {
                      echo 'Yearly Archives: ' . get_the_date('Y');
                    } else {
                      echo 'Archives:';
                    }
                  ?>
                </h2>
              </div>
            </div>
            <div class="row">
              <?php
                if(have_posts()) :
                  while (have_posts()) : the_post(); ?>
                  <div class="col col-12 col-lg-6 post-grid-container">
                    <?php get_template_part('template-parts/content-preview'); ?>
                  </div>
                <?php endwhile; ?>
                <div class="col col-12 content-pagination">
                  <?php echo paginate_links(); ?>
                </div>
                <?php  else:
                    echo '<p>Sorry, no content found</p>';
                  endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 page-share-container">
          <?php echo crunchify_social_sharing_buttons(); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer();

?>
