(function($){
  $(document).ready(function(){
    let careersArchive = document.querySelector(".post-type-archive-careers");
    let home = document.querySelector(".home");
    let locationsSingle = document.querySelector(".single-locations");
    if(careersArchive != null) {
      $('#careers-testimonials').slick({
        infinite: false,
        arrows: false,
        dots: true
      });
    }
    if(home != null) {
      $('#home-slider').slick({
        infinite: true,
        dots: false,
        prevArrow: '<i class="far fa-chevron-left"></i>',
        nextArrow: '<i class="far fa-chevron-right"></i>'
      });
    }
  });

})(jQuery);
