import "../scss/general/styles-admin.scss";

(function($){

  let mediaUploader;

  function themeSettingsMedia (backgroundImage) {
    if (mediaUploader) {
      mediaUploader.open();
      return;
    }
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Choose a Background Image',
      button: {
        text: 'Choose Image'
      },
      multiple: false
    });

    mediaUploader.on('select', function() {
      let attachment = mediaUploader.state().get('selection').first().toJSON();
      $(backgroundImage).val(attachment.url);
      $(backgroundImage + "-preview img").attr('src', attachment.url);
    });

    mediaUploader.open();
  }

  $("#careers-benefit-bgimage-button").on('click', function(e) {
    e.preventDefault();
    themeSettingsMedia("#careers-benefit-bgimage");
  });

  $("#footer-cta-1-bgimage-button").on('click', function(e) {
    e.preventDefault();
    themeSettingsMedia("#footer-cta-1-bgimage");
  });

  $("#footer-cta-2-bgimage-button").on('click', function(e) {
    e.preventDefault();
    themeSettingsMedia("#footer-cta-2-bgimage");
  });

  $("#footer-cta-3-bgimage-button").on('click', function(e) {
    e.preventDefault();
    themeSettingsMedia("#footer-cta-3-bgimage");
  });

})(jQuery);
