(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};

	// create map
	var map = new google.maps.Map( $el[0], args);

	//gets reviews
	if (document.querySelector(".single-locations")) {
		let reviewsBlock = document.querySelector('.location-reviews');
		let reviewsContainer = document.getElementById('google-reviews');
		if (reviewsContainer.dataset.placesId) {

			function addSlick() {
				$(reviewsContainer).slick({
					infinite: false,
					dots: true,
					prevArrow: '<i class="far fa-chevron-left"></i>',
					nextArrow: '<i class="far fa-chevron-right"></i>'
				});
			}

			let placesID = reviewsContainer.dataset.placesId;
			let request = {
					placeId: placesID// example: ChIJN1t_tDeuEmsRUsoyG83frY4
			};

			let service = new google.maps.places.PlacesService(map); // map is your map object

			service.getDetails(request, function(place, status) {
				if (status == google.maps.places.PlacesServiceStatus.OK) {
					let reviews = place.reviews;
					if (reviews) {
						for (let i in reviews) {
							let reviewerContainer = '<div class="reviewer-container">' + reviews[i].author_name + '</div>';
							let reviewContainer = '<div class="review-container">' + reviews[i].text + '</div>';
							let stars = reviews[i].rating;
							let starsContainer = '<div class="stars-container">';

							for (let j = 0; j < stars; j++) {
								starsContainer +='<i class="fas fa-star"></i>';
							}
							starsContainer+='</div>';

							reviewsContainer.innerHTML += '<div>' + starsContainer + reviewContainer + reviewerContainer + '</div>';
						}
						addSlick();
					} else {
						reviewsBlock.style.display = "none";
					}
				}
			});
		}

	}


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});


	// center map
	center_map( map );

	var searchBoxElement = document.getElementById('locations-search');

	if (searchBoxElement != null) {
		var searchBox = new google.maps.places.SearchBox(searchBoxElement);
		var searchMarker;
		google.maps.event.addListener(searchBox, 'places_changed', function(){

			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			console.log(places);

			for(i=0; place=places[i]; i++) {

				console.log(place.geometry.location);

				bounds.extend(place.geometry.location);

				if (searchMarker) {

					searchMarker.setPosition(place.geometry.location);

				} else {

					searchMarker = new google.maps.Marker({position: place.geometry.location});

					searchMarker.setMap(map);

				}

			}

			map.fitBounds(bounds);
			map.setZoom(13);

		});
	}

	// return
	return map;

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	var themeUrl = themeAttributes.url;

	var mapIconSize = 45;

	var mapIconAnchorX = mapIconSize / 2;

	var mapIconAnchorY = mapIconSize;

	var mapIcon = {
    url: themeUrl + '/assets/location-marker.svg', // url
    scaledSize: new google.maps.Size(mapIconSize, mapIconSize), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(mapIconAnchorX, mapIconAnchorY) // anchor
	};

	// create marker
	var marker = new google.maps.Marker({
		position: latlng,
		icon: mapIcon,
		map: map,
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	function multiZoomOut() {
		setTimeout(function() {
			map.setZoom(7);
		}, 2000);
	};

	function initialmultiZoom(zoomOut) {
		map.fitBounds( bounds );
		zoomOut();
}

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		initialmultiZoom(multiZoomOut);

	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});


})(jQuery);

let locationsArchive = document.getElementsByClassName('post-type-archive-locations')[0];
let filterCity = document.getElementById('filter-city');
let locationCards = document.querySelectorAll('[data-city-group]');

if (locationsArchive != null) {
	if (filterCity != null) {
		filterCity.addEventListener('change', function() {
			let filterValue = filterCity.value;
			for (let i = 0; i < locationCards.length; i++) {
				locationCards[i].style.display = "block";
				if (filterValue != "All Cities") {
					if (locationCards[i].dataset.cityGroup != filterValue) {
						locationCards[i].style.display = "none";
					}
				} else {
					locationCards[i].style.display = "block";
				}
			}
		});
	}
}
