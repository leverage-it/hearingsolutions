let loader = document.getElementById('page-loader');
let domBody = document.getElementsByTagName("body")[0];

window.addEventListener('DOMContentLoaded', function() {
  loader.classList.add("hidden");
  domBody.style.overflow = "initial";
});
