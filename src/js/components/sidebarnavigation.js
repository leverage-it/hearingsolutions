let sidebarNav = document.querySelectorAll('.sidebar-nav div[class*="-sidebar-menu-container"]>.menu');

//Sidebar Navigation
if (sidebarNav.length > 0) {
  //Checks How Many Sidebar Menus there are
  for (let i = 0; i < sidebarNav.length; i++) {
    let subMenus = sidebarNav[i].querySelectorAll(".sub-menu");
    let sidebarNavItem = sidebarNav[i].querySelectorAll(":scope > .menu-item");
    //Applies default active state to the parent menu of the current page
    for (let j = 0; j < subMenus.length; j++) {
      let subMenuItems = subMenus[j].querySelectorAll(".menu-item");
      for (let k = 0; k < subMenuItems.length; k++) {
        if (subMenuItems[k].classList.contains("current-menu-item") || subMenuItems[k].classList.contains("current-menu-parent")) {
          subMenuItems[k].closest(".sub-menu").closest(".menu-item").classList.add("active");
        }
      }
    }
    //Accordion effect for the sidebar navigation
    for (let j = 0; j < sidebarNavItem.length; j++) {
      if (sidebarNavItem[j].querySelector(".sub-menu") != null) {
        sidebarNavItem[j].querySelector("a").addEventListener('click', function(e) {
          e.preventDefault();
          if (!sidebarNavItem[j].classList.contains("active")) {
            for (let k = 0; k < sidebarNavItem.length; k++) {
              if (sidebarNavItem[k].classList.contains("active")) {
                sidebarNavItem[k].classList.remove("active");
              }
            }
            sidebarNavItem[j].classList.add("active");
          }
        });
      }
    }
  }
}
