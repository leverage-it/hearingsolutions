let searchIcon = document.getElementById('search-icon');
let searchForm = document.getElementById('searchform');
let searchInput = document.getElementById('s');

//Search Icon
searchIcon.addEventListener('click', function() {
  this.classList.toggle("active");
  searchForm.classList.toggle("active");
  searchInput.focus();
});
