//Header
let siteHeader = document.querySelector('.site-header');
let headerHeight = siteHeader.offsetHeight;
//logo
let brandingContainer = document.querySelector('.branding-container');
//Desktop Navigation
let mainMenu = document.querySelector('.main-nav-container');
let mainMenuItem = mainMenu.querySelectorAll('.nav .menu>.menu-item');
//Mobile Naviation
let mobileMenuButton = document.getElementById('mobile-menu-button');
let mobileMenu = document.getElementById('mobile-menu');
let mobileMenuItem = mobileMenu.querySelectorAll('.nav .menu>.menu-item');
//Load and Resize Event Checks
let desktopScrolledNavigationTrigger = false;
let mobileNavigationTrigger = false;
let mobileDropdownNavigationTrigger = false;

function desktopScroll() {
  if (window.pageYOffset > headerHeight) {
    siteHeader.classList.add("scrolled");
    if (window.matchMedia("(min-width: 768px)").matches) {
      mainMenu.classList.remove("col-lg-12");
      mainMenu.classList.add("col-md-9");
      brandingContainer.classList.remove("col-md-4");
      brandingContainer.classList.add("col-md-3");
    } else {
      mainMenu.classList.remove("col-sm-8");
      mainMenu.classList.add("col-sm-9");
      brandingContainer.classList.remove("col-sm-4");
      brandingContainer.classList.add("col-sm-3");
    }
  } else {
    siteHeader.classList.remove("scrolled");
    if (window.matchMedia("(min-width: 768px)").matches) {
      brandingContainer.classList.remove("col-md-3");
      brandingContainer.classList.add("col-md-4");
      mainMenu.classList.remove("col-md-9");
      mainMenu.classList.add("col-lg-12");
    } else {
      brandingContainer.classList.remove("col-sm-3");
      brandingContainer.classList.add("col-sm-4");
      mainMenu.classList.remove("col-sm-8");
      mainMenu.classList.add("col-sm-9");
    }
  }
}

function addDesktopScrolledNavigation() {
  window.addEventListener('scroll', desktopScroll);
  desktopScrolledNavigationTrigger = true;
}

function removeDesktopScrolledNavigation() {
  window.removeEventListener('scroll', desktopScroll);
  desktopScrolledNavigationTrigger = false;
}

function mobileNavigation() {
  mobileMenu.classList.toggle("open");
}

function addMobileNavigation() {
  mobileMenuButton.addEventListener('click', mobileNavigation);
  mobileNavigationTrigger = true;
}

function removeMobileNavigation() {
  mobileMenuButton.removeEventListener('click', mobileNavigation);
  mobileNavigationTrigger = false;
}

function addMobileDropdownNavigation() {
  for (let i = 0; i < mobileMenuItem.length; i++) {
    mobileMenuItem[i].addEventListener('click', function() {
      if (window.matchMedia("(max-width: 768px)").matches) {
        this.classList.toggle("active");
      }
    });
  }
  mobileDropdownNavigationTrigger = true;
}

//removeMobileDropDownNavigation

function hasActiveCheck(domElement) {
  for (let j = 0; domElement.length; j++) {
    return domElement[j].classList.contains("active");
  }
}
function removeActives(domElement) {
  for (let j = 0; j < domElement.length; j++) {
    if (domElement[j].classList.contains("active")) {
      domElement[j].classList.remove("active");
    }
  }
}


window.addEventListener('load', function() {
  if (window.matchMedia("(min-width: 769px)").matches) {
    addDesktopScrolledNavigation();
  } else {
    addMobileNavigation();
    addMobileDropdownNavigation();
  }
});

window.addEventListener('resize', function() {
  if (window.matchMedia("(min-width: 769px)").matches) {
    if (mobileNavigationTrigger == true) {
      removeMobileNavigation();
    }
    if (mobileDropdownNavigationTrigger == true) {
      for (let i = 0; i < mobileMenuItem.length; i++) {
        if (hasActiveCheck(mobileMenuItem)) {
          removeActives(mobileMenuItem);
        }
      }
      mobileDropdownNavigationTrigger == false;
    }
    if (desktopScrolledNavigationTrigger == false) {
      addDesktopScrolledNavigation();
    }
  } else {
    if (desktopScrolledNavigationTrigger == true) {
      removeDesktopScrolledNavigation();
    }
    if (mobileNavigationTrigger == false) {
      addMobileNavigation();
    }
    if (mobileDropdownNavigationTrigger == false) {
      addMobileDropdownNavigation();
    }
  }
});
