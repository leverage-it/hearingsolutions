let careersArchive = document.getElementsByClassName('post-type-archive-careers')[0];
let filterCity = document.getElementById('filter-city');
let filterPosition = document.getElementById('filter-position');
let careersCity = document.querySelectorAll('[data-city]');
let careersPosition = document.querySelectorAll('[data-position]');
let jobListing = document.querySelectorAll('.job-listing');
let jobListingActive = document.querySelectorAll('.job-listing.active');
let resetButton = document.getElementById('filter-reset');
let noJobMessage = document.getElementById('nojobs');

function removeBlueLine() {
  jobListingActive = document.querySelectorAll('.job-listing.active');
  for (let j = 0; j < jobListingActive.length; j++) {
    if (j == jobListingActive.length-1) {
      jobListingActive[j].style.borderBottom = "none";
    }
  }
}
function cityCheck() {
  let cityValue = filterCity.value;
  let positionValue = filterPosition.value;
  if (positionValue != 'Position') {
    for (let i = 0; i < jobListing.length; i++) {
      if (jobListing[i].classList.contains("active")) {
        jobListing[i].classList.remove("active");
      }
      if (jobListing[i].dataset.city == cityValue && jobListing[i].dataset.position == positionValue) {
        jobListing[i].classList.add("active");
      }
    }
  } else {
    for (let i = 0; i < jobListing.length; i++) {
      if (jobListing[i].classList.contains("active")) {
        jobListing[i].classList.remove("active");
      }
      if (jobListing[i].dataset.city == cityValue) {
        jobListing[i].classList.add("active");
      }
    }
  }
}
function positionCheck() {
  let positionValue = filterPosition.value;
  let cityValue = filterCity.value;
  if (cityValue != 'City') {
    for (let i = 0; i < jobListing.length; i++) {
      if (jobListing[i].classList.contains("active")) {
          jobListing[i].classList.remove("active");
      }
      if (jobListing[i].dataset.position == positionValue && jobListing[i].dataset.city == cityValue) {
        jobListing[i].classList.add("active");
      }
    }
  } else {
    for (let i = 0; i < jobListing.length; i++) {
      if (jobListing[i].classList.contains("active")) {
          jobListing[i].classList.remove("active");
      }
      if (jobListing[i].dataset.position == positionValue) {
        jobListing[i].classList.add("active");
      }
    }
  }
}
function jobAvailableCheck() {
  jobListingActive = document.querySelectorAll('.job-listing.active');
  if (jobListingActive.length == 0) {
    noJobMessage.classList.add("active");
  } else {
    noJobMessage.classList.remove("active");
  }
}

if (careersArchive != null) {
  filterCity.addEventListener('change', function() {
    cityCheck();
    jobAvailableCheck();
    removeBlueLine();
  });

  filterPosition.addEventListener('change', function() {
    positionCheck();
    jobAvailableCheck();
    removeBlueLine();
  });

  resetButton.addEventListener('click', function(e) {
    e.preventDefault();
    filterCity.value = 'City';
    filterPosition.value = 'Position';
    for (let i = 0; i < jobListing.length; i++) {
      if(!jobListing[i].classList.contains('active')) {
        jobListing[i].classList.add('active');
      }
    }
    jobListingActive = document.querySelectorAll('.job-listing.active');
    for (let j = 0; j < jobListingActive.length; j++) {
      jobListingActive[j].style.borderBottom = "1px solid #A4CEEE";
      if (j == jobListingActive.length-1) {
        jobListingActive[j].style.borderBottom = "none";
      }
    }
    noJobMessage.classList.remove("active");
  });
}
