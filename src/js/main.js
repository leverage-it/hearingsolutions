//Jarallax Plugin
import {
  jarallax,
  jarallaxElement
} from 'jarallax';
jarallaxElement();

jarallax(document.querySelectorAll('.jarallax'), {
  speed: 0.2
});

import './components/loader';
import './components/announcementbar';
import './components/search';
import './components/headernavigation';
import './components/sidebarnavigation';
import './components/careers';
