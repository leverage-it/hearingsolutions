<?php

  get_header();

  $hero = get_field('careers_hero', 'option');
  $hero_image = $hero['careers_hero_image'];
  $hero_title = $hero['careers_hero_title'];
  $hero_text = $hero['careers_hero_text'];
  $hero_button_text = $hero['careers_hero_button_text'];
  $hero_button_url = $hero['careers_hero_button_url'];

  $testimonials = get_field('careers_testimonials', 'option');
  $testimonials_title = $testimonials['careers_testimonials_title'];
  $testimonials_entries = $testimonials['careers_testimonials_entries'];

  $job_listings = get_field('careers_job_listings', 'option');
  $job_listings_title = $job_listings['careers_job_sections_title'];
?>

<section class="container-fluid page-header-container jarallax">
  <img src="<?php echo $hero_image; ?>" class="jarallax-img page-header-image" alt="">
  <div class="row breadcrumb-container-transparent">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col col-12 page-title-container">
          <div class="page-title">
            <h1 class="page-title-text"> <?php echo $hero_title; ?></h1>
            <div class="line"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 page-excerpt-container">
          <div class="page-excerpt">
            <h4>
              <?php echo $hero_text;?>
            </h4>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 page-header-cta-container">
          <?php
            if ($hero_button_text && $hero_button_url) {
          ?>
            <a href="<?php echo $hero_button_url; ?>" class="button-blue"><?php echo $hero_button_text; ?></a>
          <?php
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid offset-container for-container">
  <div class="row">
    <div class="container sheet-container testimonial-container top">
      <div class="row">
        <div class="col col-12 testimonial-title-container">
          <div class="section-header">
            <h3><?php echo $testimonials_title; ?></h3>
            <div class="line"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 testimonial-text-container">
          <div id="careers-testimonials" class="testimonials">
            <?php
              if ($testimonials_entries):
                foreach ($testimonials_entries as $entries) {
            ?>
            <div class="row justify-content-start">
              <div class="col col-12 col-md-4 avatar-container">
                <img src="<?php echo $entries['careers_testimonials_avatar']; ?>" class="avatar" alt="Image of <?php echo $entries['careers_testimonials_author']; ?>">
              </div>
              <div class="col col-2 col-md-1">
                <span class="quote">&ldquo;</span>
              </div>
              <div class="col-10 col-md-6 testimonial-text-container">
                <div class="row">
                  <div class="col-12 testimonial-text">
                    <?php echo $entries['careers_testimonials_text']; ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 testimonial-author">
                    -<?php echo $entries['careers_testimonials_author']; ?>
                  </div>
                </div>
              </div>
            </div>
            <?php
                }
              endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_template_part('template-parts/careers-features'); ?>
<section class="container-fluid offset-container for-container">
  <div class="row">
    <div class="container sheet-container job-listings-container bottom">
      <div class="row">
        <div class="col col-12 job-listings-title-container">
          <div class="section-header">
            <h3><?php echo $job_listings_title; ?></h3>
            <div class="line"></div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col col-10 col-md-8 list-filters">
          <div class="row filters-dropdown-container">
              <div class="col col-12 col-md-5 filters-city-container vertical-center">
                <div class="select">
					<label for="filter-city" class="visually-hidden">Filter by city</label>
                  <select id="filter-city">
                    <option selected="true" disabled="disabled">City</option>
                  <?php
                    if(have_posts()) :
                      $careers_cities = [];
                      while(have_posts()) : the_post();
                        if(get_the_title() != 'Apply') {
                          $careers_cities_group = get_field('career_city');

                          if(!in_array($careers_cities_group, $careers_cities)) {
                            $careers_cities[] = $careers_cities_group;
                            sort($careers_cities);
                          }
                        }
                      endwhile;

                      foreach ($careers_cities as $city) {
                  ?>
                    <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                  <?php
                      }
                    endif;
                  ?>
                  </select>
                </div>
              </div>
              <div class="col col-12 col-md-5 filters-position-container vertical-center">
                <div class="select">
					<label for="filter-position" class="visually-hidden">Filter by position</label>
                  <select id="filter-position">
                    <option selected="true" disabled="disabled">Position</option>
                  <?php
                    if(have_posts()) :
                      $careers_positions = [];
                      while(have_posts()) : the_post();
                        if(get_the_title() != 'Apply') {
                          $careers_positions_group = get_field('career_job_position');

                          if(!in_array($careers_positions_group, $careers_positions)) {
                            $careers_positions[] = $careers_positions_group;
                            sort($careers_positions);
                          }
                        }
                      endwhile;

                      foreach ($careers_positions as $positions) {
                  ?>
                    <option value="<?php echo $positions; ?>"><?php echo $positions; ?></option>
                  <?php
                      }
                    endif;
                  ?>
                  </select>
                </div>
              </div>
              <div class="col col-12 col-md-2 filters-reset-button-container">
				  <button type="button" name="button" id="filter-reset" class="button-blue filter-reset-button" aria-label="Rest filter fields"><i class="far fa-sync-alt"></i></button>
              </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col col-10 list-container">
          <?php

            if(have_posts()) :
            while (have_posts()) : the_post();
            if (get_the_title() != 'Apply') {

              $career_city = get_field('career_city');
              $career_position = get_field('career_job_position');

              ?>
              <div class="row job-listing active" data-city="<?php echo $career_city; ?>" data-position="<?php echo $career_position; ?>">
                <div class="col col-12 col-md-5 col-lg-7 vertical-center title">
                    <h4><?php the_title(); ?></h4>
                </div>
                <div class="col col-12 col-md-7 col-lg-5">
                  <div class="row">
                    <div class="col col-6 vertical-center city"><h4><?php echo $career_city; ?></h4></div>
                    <div class="col col-6"><a class="button-blue" href="<?php the_permalink(); ?>">See Position</a></div>
                  </div>
                </div>
              </div>
          <?php
              }
              endwhile;
              else:
                echo '<p>Sorry, no content found</p>';
              endif;
          ?>
        </div>
      </div>
      <div class="row justify-content-center">
        <div id="nojobs" class="col col-10">
          <h3>No Jobs Found</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="row linkedin-follow">
    <!--Come back to fill in this section with Linked In-->
  </div>
</section>

<?php

  get_footer();

?>
