<?php

get_header();

  $blog_params = array(
    'posts_per_page' => '1'
  );

$header_query = new WP_Query($blog_params);
while($header_query->have_posts()) : $header_query->the_post();
?>

<section class="container-fluid blog-header-container jarallax">
  <img src="<?php echo get_the_post_thumbnail_url(null, 'feed-header-thumbnail'); ?>" class="jarallax-img page-header-image" alt="">
  <div class="row breadcrumb-container-transparent">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
  <div class="row blog-header-titles">
    <div class="container">
      <div class="row">
        <div class="col col-12 col-md-8">
          <p class="latest-post">latest post</p>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 col-md-8">
          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 col-md-8">
          <p class="blog-header-titles-meta"><?php the_time('F jS, Y');?> by <a class="author-link" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
endwhile;
wp_reset_query();
?>

<section class="container-fluid main-container">
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="col col-12 index-content-container">
        <div class="content">
          <div class="row">
            <?php
              if(have_posts()) :
                while (have_posts()) : the_post(); ?>
                <div class="col col-12 col-lg-6 post-grid-container">
                  <?php get_template_part('template-parts/content-preview'); ?>
                </div>
              <?php endwhile; ?>
              <div class="col col-12 content-pagination-container">
                <div class="row justify-content-center">
                  <div class="col col-12 col-md-4">
                    <div class="content-pagination">
                      <?php
                        $paginate_links_params = array(
                          'prev_next' => 'true',
                          'prev_text' => '<',
                          'next_text' => '>'
                        );
                        echo paginate_links($paginate_links_params);
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php  else:
                  echo '<p>Sorry, no content found</p>';
                endif; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-12 page-share-container">
        <?php echo crunchify_social_sharing_buttons(); ?>
      </div>
    </div>
    </div>
  </div>
  <div class="row quick-container-container">
    <div class="container">
      <div class="row">
        <div class="col col-12">
          <?php gravity_form('4', true, true, false, false, true, '', true); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php

  get_footer();

?>
