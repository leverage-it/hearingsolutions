<?php

get_header();

?>

<section class="container-fluid page-header-container jarallax">
  <img class="jarallax-img" src="<?php bloginfo('template_directory'); ?>/assets/img/product_header_image.jpg">
  <div class="row breadcrumb-container-transparent">
    <?php get_template_part('template-parts/breadcrumb'); ?>
  </div>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col col-12 page-title-container">
          <div class="page-title">
            <h1 class="page-title-text">
              <?php
                single_term_title();
              ?>
            </h1>
            <div class="line"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-12 page-excerpt-container">
          <div class="brand-excerpt">
            <?php echo term_description(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid main-container">
  <div class="row">
    <div class="container content-container">
      <div class="row">
        <div class="col col-12 product-content-container">
          <div class="content">
            <div class="row">
              <?php
                if(have_posts()) :

                  //Conditional Sub Section Header
                  $product_posts = [];
                  while(have_posts()) : the_post();
                    $latest_product = get_field('product_latest_product');
                    if (!in_array($latest_product, $product_posts)) {
                      $product_posts[] = $latest_product;
                    }
                  endwhile;
                  ?>

                  <div class="col col-12 product-subtitle-container">
                    <?php
                      if(in_array(1, $product_posts)) {
                        echo "<div class='section-header'><h3>Latest ";
                        single_term_title();
                        echo " Hearing Aid Models</h3><div class='line'></div></div>";
                      }
                    ?>
                  </div>

                  <?php
                  //End of Conditional Sub Section Header

                  while(have_posts()) : the_post();
                  $latest_product = get_field('product_latest_product');
                    if ($latest_product == 1) :
              ?>
              <div class="col col-12 col-md-6 col-lg-4 post-grid-container">
                <?php get_template_part('template-parts/product-preview'); ?>
              </div>
              <?php
                  endif;
                endwhile;
                else:
                  echo '<p>Sorry, no content found</p>';
                endif;
              ?>
            </div>
            <div class="row">
              <?php
                if(have_posts()) :

                  //Conditional Sub Section Header
                  $product_posts = [];
                  while(have_posts()) : the_post();
                    $latest_product = get_field('product_latest_product');
                    if (!in_array($latest_product, $product_posts)) {
                      $product_posts[] = $latest_product;
                    }
                  endwhile;
                  ?>

                  <div class="col col-12 product-subtitle-container">
                    <?php
                      if(in_array(1, $product_posts)) {
                        echo "<div class='section-header'><h3>Other ";
                        single_term_title();
                        echo " Hearing Aid Models</h3><div class='line'></div></div>";
                      }
                    ?>
                  </div>

                  <?php
                  //End of Conditional Sub Section Header

                  while(have_posts()) : the_post();
                  $latest_product = get_field('product_latest_product');
                    if ($latest_product == 0) :
              ?>
              <div class="col col-12 col-md-6 col-lg-4 post-grid-container">
                <?php get_template_part('template-parts/product-preview'); ?>
              </div>
              <?php
                  endif;
                endwhile;
                else:
                  echo '<p>Sorry, no content found</p>';
                endif;
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-lg-end">
        <div class="col col-12 page-share-container">
          <?php echo crunchify_social_sharing_buttons(); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row quick-container-container">
    <div class="container">
      <div class="row">
        <div class="col col-12">
            <?php gravity_form('4', true, true, false, false, true, '', true); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php

get_footer();

?>
