<?php

  get_header();

  if(have_posts()) :
    while (have_posts()) : the_post();

?>
<section class="container-fluid blog-main-container">
  <div class="row">
    <div class="container">
      <div class="row breadcrumb-container">
        <?php get_template_part('template-parts/breadcrumb'); ?>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="container content-container">
      <div class="row">
        <aside class="col col-12 col-lg-3 page-sidebar-container">
          <nav class="sidebar-nav" aria-label="Sidebar menu">
            <?php
              $page_sidebar_nav_params = array(
                'theme_location' => 'page-sidebar-navigation'
              );
              wp_nav_menu($page_sidebar_nav_params);
            ?>
          </nav>
	  </aside>
        <article class="col col-12 col-lg-9 page-content-container">
          <div class="content">
            <div class="row blog-info">
              <div class="col col-12">
                <p class="blog-info-meta"><?php the_time('F jS, Y');?> | <?php
                  $categories = get_the_category();
                  $categories_output = '';

                  if($categories) {
                    foreach($categories as $category) {
                      $categories_output .= '<a class="post-categories" href="' . get_category_link($category->term_id) . '">' . $category->cat_name . '</a>';
                    }
                    echo $categories_output;
                  }
                  ?> | by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a>
			  </p>
              </div>
            </div>
            <div class="row blog-title">
              <div class="col col-12">
                <h1><?php the_title(); ?></h1>
              </div>
            </div>
            <div class="row blog-thumbnail">
              <div class="col col-12">
                <?php the_post_thumbnail('single-featured-image'); ?>
              </div>
            </div>
            <div class="row blog-content">
              <div class="col col-12">
                <?php the_content(); ?>
              </div>
            </div>
		</div>
	</article>
      </div>
      <div class="row justify-content-lg-end">
        <div class="col col-12 col-lg-9 page-share-container">
          <?php echo crunchify_social_sharing_buttons(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<?php

    endwhile;

    else:
  echo '<p>Sorry, no content found</p>';
  endif;

  get_footer();

?>
