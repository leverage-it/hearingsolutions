<?php

get_header(); ?>


<section class="container-fluid blog-main-container">
  <div class="row">
    <div class="container">
      <div class="row breadcrumb-container">
        <?php get_template_part('template-parts/breadcrumb'); ?>
      </div>
      <div class="row">
      <div class="col col-12 search-content-container">
        <div class="content">
          <div class="row">
            <div class="col col-12 search-title-container">
              <div class="section-header">
                <h2>
                  <?php
                    echo "Search Results for: ";
                    the_search_query();
                  ?>
                </h2>
                <div class="line"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <?php
              if(have_posts()) :
                while (have_posts()) : the_post(); ?>
                <div class="col col-12 search-results">
                  <?php get_template_part('template-parts/content-preview'); ?>
                </div>
              <?php endwhile; ?>
              <span class="col col-12 content-pagination">
                <?php echo paginate_links(); ?>
              </span>
              <?php  else:
                  echo '<p>Sorry, no content found</p>';
                endif; ?>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</section>

<?php get_footer();

?>
